<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Receber Pagamento</title>
</head>
<body>
<TagHeader:TagHead></TagHeader:TagHead> 
<TagHeader:TagSessaoAdmin></TagHeader:TagSessaoAdmin>
<c:import url="menu-admin.jsp"></c:import>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>
<h5>Voc� est� em : Receber Pagamento</h5>
<h4>Receber</h4>
<form action="CadastrarTaxa.do?ref=recebido" method="post">
<label for= "dia">Dia do pagamento:</label><input type="text" name="dia" size="2">
<label for= "mes"> M�s :</label><input type="text" name="mes" size="2"> <label for= "ano">Ano :</label><input type="text" name="ano" size="4"><br>
<label for="valor">Valor:</label><input type="text" name="valor">
<input type="hidden" value="${requestScope.taxa.id}" name="id">


<input type="submit" value="Pago">
</form>

<table class="table table-striped">
<tr>
	<td>Condomino</td>
	<td>Referencia</td>
	<td>Valor</td>
	
</tr>
<tr>
	<td>${requestScope.taxa.condomino.login }</td>
	<td><fmt:formatDate pattern="dd/MM/yyyy" 
            value="${requestScope.taxa.referencia }" /></td>
	<td><fmt:formatNumber value="${requestScope.taxa.taxa }" 
            type="currency"/></td>
	
</tr>

</table>

</body>
</html>