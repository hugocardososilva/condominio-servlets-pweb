<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" errorPage="Erro.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Administrador</title>
</head>
<body>

<TagHeader:TagHead></TagHeader:TagHead>  
<c:import url="menu-admin.jsp"/>

<TagHeader:TagSessaoAdmin></TagHeader:TagSessaoAdmin>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>
<h5>Voc� est� em : In�cio</h5>
<p>Bem vindo, ${sessionScope.admin.nome }</p>
<p> Use o menu lateral para cadastrar, visualizar, gerar taxas, entre outros.</p>
<p> Para fazer logoff, clique em Logoff.</p>
</body>
</html>