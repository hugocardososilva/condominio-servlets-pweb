<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" errorPage="Erro.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Gerenciar Colaborador</title>
</head>
<body>
<TagHeader:TagSessaoAdmin></TagHeader:TagSessaoAdmin>
<TagHeader:TagHead></TagHeader:TagHead>
<c:import url="menu-admin.jsp"></c:import>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>
<h5>Voc� est� em : Gerenciar Colaborador</h5>
<p>Nome:     ${requestScope.colaborador.nome }</p>
<p>CPF / CNPJ:     ${requestScope.colaborador.cpf }</p>
<p>Telefone:    ${requestScope.colaborador.telefone }</p>
<p>Servi�o:    ${requestScope.colaborador.servico }</p>
<table class="table table-striped">
<thead>Horarios</thead>
<c:choose>
<c:when test="${requestScope.colaborador.horarios != null }">

	<tr>
		
		
	
	
	</tr>
	<c:forEach var="horario" items="${requestScope.colaborador.horarios }">
		<tr>
			
			<td>${horario.data }</td>
			<td>${horario.condomino.login }</td>
		</tr>
	
	</c:forEach>
	</c:when>
</c:choose>
<tr><td><a href="<c:url value='/AdministrarHorarios.do?ref=add&id=${colaborador.id }'/>"><button class="btn btn-primary" type="button"> Adicionar Atendimento</button></a></td>

</tr>

</table>
</body>

</html>