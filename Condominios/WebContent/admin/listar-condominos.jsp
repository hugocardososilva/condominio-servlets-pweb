<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" errorPage="Erro.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cond�minos</title>
</head>

<body style="width: 1400px; clear: both;">
<TagHeader:TagSessaoAdmin></TagHeader:TagSessaoAdmin>

<TagHeader:TagHead></TagHeader:TagHead> 
<c:import url="menu-admin.jsp"></c:import>	
<%-- <p>${requestScope.lista }</p> --%>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>
<h5>Voc� est� em : Listar Condominos</h5>
<table class="table table-striped" style="max-width: 1000px; clear:both;">
<tr style="font-weight: bold;">
				<td>ID</td>
				<td>Login</td>
			
				<td>Proprietario</td>
			
				<td>Apartamento</td>
			
				<td>Email</td>
				<td>Telefone</td>
				<td>Inquilino</td>
				
				<td>Aprovado</td>
				
				<td>Moradores</td>
				<td>Ve�culos</td>
			</tr>
	<c:forEach var="condomino" items="${requestScope.lista}">
	<tr>
		<td>${condomino.id }</td>
		<td>${condomino.login }</td>
		<td>${condomino.proprietario }</td>
		<td>${condomino.apartamento }</td>
		<td>${condomino.email }</td>
		<td>${condomino.telefone }</td>
		<td>${condomino.inquilino }</td>
		<c:choose>
		<c:when test="${condomino.revisado == false }">
		<td>
		<a href="<c:url value='/AdministrarCondomino.do?ref=aprovar&id=${condomino.id }'/>"><button class="btn btn-success" type="button"> Aprovar</button></a>
		 </td>
		</c:when>
		<c:otherwise>
		<td><span class="label label-success">Sim</span></td>
		</c:otherwise>
		</c:choose>
		
		
		<td>
		
		<c:forEach var="morador" items="${condomino.moradores}">
		<table>
		<tr>
		<td>Nome: ${morador.nome }</td>
		</tr>
		</table>
		</c:forEach>
		
		</td>
		<td>
		<c:forEach var="veiculo" items="${condomino.veiculos}">
		<table>
		<tr><td>Cor: ${veiculo.cor }</td></tr>
		<tr><td>Placa: ${veiculo.placa }</td></tr>
		<tr><td>Modelo: ${veiculo.modelo }</td></tr>
		<tr><td>Marca: ${veiculo.marca }</td></tr>
		</table>
		</c:forEach>
		
		</td>
		
		
	
	
	</tr>	
	
	</c:forEach>
	</table>

</body>
</html>