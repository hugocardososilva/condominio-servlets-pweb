<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Enviar Mensagem</title>
</head>
<body>
<TagHeader:TagSessaoAdmin></TagHeader:TagSessaoAdmin>
<TagHeader:TagHead></TagHeader:TagHead>
<c:import url="menu-admin.jsp"></c:import>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>
<h5>Voc� est� em : Administrar Mensagens</h5>


<form action="/Condominios/AdministrarMensagens.do?ref=add" method="post">
<label  for="condomino">Para:  </label>
<select name="condomino" multiple="multiple" size="10" style="widht:150px; height: 200px; ">
<c:forEach var="condomino" items="${sessionScope.condominos }">
<c:if test="${condomino.revisado != false }">
<option value="${condomino.id }">${condomino.login }</option>
</c:if>
</c:forEach>
</select><br>
<label  for="titulo">T�tulo:</label> <input type="text" name="titulo"><br>
<label  for="assunto">Assunto:</label> <input type="text" name="assunto"><br>
<label  for="condomino">Mensagem:</label> <textarea name="mensagem" rows="5" cols="20" ></textarea><br>
<input type="submit" value="Enviar">
</form>

<table class="table table-striped">
<tr>
				<td>ID</td>
				
				<td>Titulo</td>
			
				<td>Assunto</td>
			
				<td>Mensagem</td>
				<td>Excluir</td>
				
				
</tr>
<c:forEach var="msg" items="${requestScope.mensagens }">
		<tr>
		<td>${msg.id }</td>
		<td>${msg.titulo }</td>
		<td>${msg.assunto }</td>
		<td>${msg.mensagem }</td>
			
		<td><a href="<c:url value='/AdministrarMensagens.do?ref=excluir&id_mensagem=${msg.id }'/>"><button class="btn btn-danger" type="button"> Excluir</button></a></td>
		
	</tr>
	</c:forEach>

</table>
</body>
</html>