<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Painel de inadipl�ncia</title>
</head>
<body>
<TagHeader:TagSessaoAdmin></TagHeader:TagSessaoAdmin>
<TagHeader:TagHead></TagHeader:TagHead>
<c:import url="menu-admin.jsp"></c:import>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>
<h5>Voc� est� em : Painel de taxas</h5>
<form action="ServletAdmin.do" method="get">
<label for="ano">Ano:</label>
<select name="ano" size="1" >
	
	<option value="2013">2013</option>
	<option value="2014">2014</option>
	</select>
<input type= "hidden" name="ref" value="painel">
<input type="submit" value="Ir"> 
</form>

<c:choose>
<c:when test="${requestScope.paineis != null}">
<table class="table ">
<tr>
	<td>Apto</td>
	<td>Saldo devedor</td>
	<td>Jan</td>
	<td>Fev</td>
	<td>Mar</td>
	<td>Abr</td>
	<td>Mai</td>
	<td>Jun</td>
	<td>Jul</td>
	<td>Ago</td>
	<td>Set</td>
	<td>Out</td>
	<td>Nov</td>
	<td>Dez</td>
</tr>
<c:forEach items="${requestScope.paineis}" var="painel">
<tr>
	<td>${painel.condomino.apartamento }</td>
	<td><fmt:formatNumber value="${painel.atraso }" 
            type="currency"/></td>
	
	<c:forEach items="${painel.taxas }" var="taxa">
		
		<c:choose>
			<c:when test="${taxa.key == '1' }">
			<c:if test="${taxa.value.pago eq true }">
			<td><span class="label label-success">Sim</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq false }">
			<td><span class="label label-important">N�o</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq null }">
			<td></td>
			</c:if>
			</c:when>
				
			
			<c:when test="${taxa.key == '2' }">
			<c:if test="${taxa.value.pago eq true }">
			<td><span class="label label-success">Sim</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq false }">
			<td><span class="label label-important">N�o</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq null }">
			<td></td>
			</c:if>
			</c:when>
				
			
			<c:when test="${taxa.key == '3' }">
			<c:if test="${taxa.value.pago eq true }">
			<td><span class="label label-success">Sim</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq false }">
			<td><span class="label label-important">N�o</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq null }">
			<td></td>
			</c:if>
			</c:when>
				
			
			<c:when test="${taxa.key == '4' }">
			<c:if test="${taxa.value.pago eq true }">
			<td><span class="label label-success">Sim</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq false }">
			<td><span class="label label-important">N�o</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq null }">
			<td></td>
			</c:if>
			</c:when>
				
			</c:choose>
			<c:choose>
			<c:when test="${taxa.key == '5' }">
			<c:if test="${taxa.value.pago eq true }">
			<td><span class="label label-success">Sim</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq false }">
			<td><span class="label label-important">N�o</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq null }">
			<td></td>
			</c:if>
			</c:when>
				
			
			<c:when test="${taxa.key == '6' }">
			<c:if test="${taxa.value.pago eq true }">
			<td><span class="label label-success">Sim</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq false }">
			<td><span class="label label-important">N�o</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq null }">
			<td></td>
			</c:if>
			</c:when>
				
			
			<c:when test="${taxa.key == '7' }">
			<c:if test="${taxa.value.pago eq true }">
			<td><span class="label label-success">Sim</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq false }">
			<td><span class="label label-important">N�o</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq null }">
			<td></td>
			</c:if>
			</c:when>
				
			
			<c:when test="${taxa.key == '8' }">
			<c:if test="${taxa.value.pago eq true }">
			<td><span class="label label-success">Sim</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq false }">
			<td><span class="label label-important">N�o</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq null }">
			<td></td>
			</c:if>
			</c:when>
				
			
			<c:when test="${taxa.key == '9' }">
			<c:if test="${taxa.value.pago eq true }">
			<td><span class="label label-success">Sim</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq false }">
			<td><span class="label label-important">N�o</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq null }">
			<td></td>
			</c:if>
			</c:when>
				
			
			<c:when test="${taxa.key == '10' }">
			<c:if test="${taxa.value.pago eq true }">
			<td><span class="label label-success">Sim</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq false }">
			<td><span class="label label-important">N�o</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq null }">
			<td></td>
			</c:if>
			</c:when>
				
		
			<c:when test="${taxa.key == '11' }">
			<c:if test="${taxa.value.pago eq true }">
			<td><span class="label label-success">Sim</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq false }">
			<td><span class="label label-important">N�o</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq null }">
			<td></td>
			</c:if>
			</c:when>
				
			
			<c:when test="${taxa.key == '12' }">
			<c:if test="${taxa.value.pago eq true }">
			<td><span class="label label-success">Sim</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq false }">
			<td><span class="label label-important">N�o</span></td>
			</c:if>
			<c:if test="${taxa.value.pago eq null }">
			<td></td>
			</c:if>
			</c:when>
				
			</c:choose>
		</c:forEach>
</tr>

</c:forEach>
<tr>
	<td>Total em atraso</td>
	<td><fmt:formatNumber value="${requestScope.total_atrasado}" 
            type="currency"/></td>
            <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
</tr>
<tr>
	<td>Total recebido</td>
	<td><fmt:formatNumber value="${requestScope.total_receita}" 
            type="currency"/></td>
            <td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td><td></td>
</tr>
</table>


</c:when>
<c:otherwise>

</c:otherwise>
</c:choose>
</body>
</html>