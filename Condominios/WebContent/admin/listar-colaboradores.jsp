<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" errorPage="Erro.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title></title>
</head>
<body>
<TagHeader:TagSessaoAdmin></TagHeader:TagSessaoAdmin>

<TagHeader:TagHead></TagHeader:TagHead>  
<c:import url="menu-admin.jsp"></c:import>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>
<h5>Voc� est� em : Listar Colaboradores</h5>
<form action="/AdministrarColaborador">
<input type="hidden" value="por_nome">
<label for="nome">Pesquisar por nome:</label>
<input type="text" name="nome">
<input type="submit" value="Pesquisar">
</form>
<table class="table table-striped" style="max-width: 800px; clear:both;">
<tr>
				<td>ID</td>
				
				<td>Nome</td>
			
				<td>CPF/CNPJ</td>
			
				<td>Telefone</td>
			
				<td>Servi�o</td>
				
				<td>Editar</td>
				
<!-- 				<td>Remover</td> -->
				
				<td>Atendimentos</td>
				
				
			</tr>
	<c:forEach var="colaborador" items="${requestScope.colaboradores}">
	<tr>
		<td>${colaborador.id }</td>
		<td>${colaborador.nome }</td>
		<td>${colaborador.cpf }</td>
		<td>${colaborador.telefone }</td>
		<td>${colaborador.servico }</td>
		<td><a href="<c:url value='/AdministrarColaborador.do?ref=editar&id=${colaborador.id }'/>"><button class="btn btn-primary" type="button"> Editar</button></a></td>
<%-- 		<td><a href="<c:url value='/AdministrarColaborador.do?ref=remover&id=${colaborador.id }'/>"> Remover</a></td> --%>
		<td><a href="<c:url value='/AdministrarColaborador.do?ref=gerenciar&id=${colaborador.id }'/>"><button class="btn btn-primary" type="button">Gerenciar</button></a></td>
		

	
	
	</tr>	
	
	</c:forEach>
	</table>

</body>
</html>