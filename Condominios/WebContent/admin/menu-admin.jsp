<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<div class="navbar navbar-inverse">

 <div class="navbar-inner">



<ul class="nav nav-pills">
<li><a href="<c:url value='/ServletAdmin.do?ref=inicio'/>"> In�cio</a></li>
	
	<li><a href="<c:url value='/AdministrarCondomino.do?ref=listar'/>"> Listar Cond�mino</a></li>
	<li><a href="<c:url value='/CadastrarColaborador.do?ref=add'/>"> Cadastrar colaborador</a></li>
	<li><a href="<c:url value='/AdministrarColaborador.do?ref=listar'/>"> Listar Colaboradores</a></li>
 	<li><a href="<c:url value='/AdministrarMensagens.do?ref=administrar'/>">Administrar mensagens</a></li> 
	<li><a href="<c:url value='/CadastrarTaxa.do?ref=visualisar'/>"> Taxa de condom�nio</a></li>
	<li><a href="<c:url value='/ServletAdmin.do?ref=inadiplencia'/>"> Painel</a></li>
	<li><a href="<c:url value='/Logoff.do'/>"><span class="label label-warning">Fazer Logoff</span></a></li>
	</ul>	
	<c:choose>
<c:when test="${sessionScope.admin == null }">
<c:import url="login-admin.jsp"></c:import>
</c:when>
<c:otherwise>
<div id="login"> ${sessionScope.admin.email }</div>
</c:otherwise>

</c:choose>
</div>

</div>