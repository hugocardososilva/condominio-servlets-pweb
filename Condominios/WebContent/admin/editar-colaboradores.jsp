<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" errorPage="Erro.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Editar Colaborador</title>
</head>
<body>
<TagHeader:TagSessaoAdmin></TagHeader:TagSessaoAdmin>
<TagHeader:TagHead></TagHeader:TagHead>
<c:import url="menu-admin.jsp"></c:import>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>
<h5>Voc� est� em : Editar Colaborador</h5>
<form action="AdministrarColaborador.do?ref=editarCol&id=${requestScope.colaborador.id }" method="post">
Nome: <input type="text" name="nome" value="${requestScope.colaborador.nome }"><br>
Cpf: <input type="text" name="cpf" value="${requestScope.colaborador.cpf }"><br>
Telefone: <input type="text" name="telefone" value="${requestScope.colaborador.telefone }"><br>
Servi�o: <input type="text" name="servico" value="${requestScope.colaborador.servico }"><br>
<input type="submit" value="Editar">
</form>
</body>
</html>