<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" errorPage="Erro.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Taxas</title>
</head>
<body>
<TagHeader:TagSessaoAdmin></TagHeader:TagSessaoAdmin>

<TagHeader:TagHead></TagHeader:TagHead> 
<c:import url="menu-admin.jsp"></c:import>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>
<h5>Voc� est� em : Taxas e pagamentos</h5>
<h4>Gerar novo m�s</h4>
<form action="CadastrarTaxa.do?ref=gerar" method="post">
<label for= "dia">Dia do vencimento:</label><input type="text" name="dia" size="2"><br>
<label for= "mes"> M�s refer�ncia:</label><input type="text" name="mes" size="2"> <label for= "ano">Ano refer�ncia:</label><input type="text" name="ano" size="4"><br>
<label for="valor">Valor:</label><input type="text" name="valor"><br>
<label for= "multa">Multa(ao dia):</label><input type="text" name="multa"><br>

<input type="submit" value="Gerar">
</form>
<form action="CadastrarTaxa.do?ref=pdf" method="post">
	<label for="ano">ano</label>
	<select name="ano" size="1" >
	
	<option value="2014">2014</option>
	
	</select>
	<input type="submit" value="Imprimir em Pdf">
</form>
<div id="taxa-mes">
<p>Selecione o m�s que deseja ver</p>
<form action="CadastrarTaxa.do?ref=visualizar_mes" method="post">
	<label for="mes">M�s</label>
	<select name="mes" size="1" >
	<option value="01/01/2014">Janeiro/2014</option>
	<option value="01/02/2014">Fevereiro/2014</option>
	<option value="01/03/2014">Mar�o/2014</option>
	<option value="01/04/2014">Abril/2014</option>
	<option value="01/05/2014">Maio/2014</option>
	<option value="01/06/2014">Junho/2014</option>
	<option value="01/07/2014">Julho/2014</option>
	<option value="01/08/2014">Agosto/2014</option>
	<option value="01/09/2014">Setembro/2014</option>
	<option value="01/10/2014">Outubro/2014</option>
	<option value="01/11/2014">Novembro/2014</option>
	<option value="01/12/2014">Dezembro/2014</option>
	</select>
	<input type="submit" value="Filtrar">
	
</form>
</div>
<div>
<table class="table table-striped">
<tr>
	<td>Cond�mino</td>
	<td>Referencia</td>
	<td>Valor</td>
	<td>Multa</td>
	<td>Pago ?</td>
	
	<td>Data pagamento</td>
	<td>Excluir</td>
	
</tr>
<c:forEach var="taxa" items="${requestScope.taxas }">
<tr>
	<td>${taxa.condomino.login }</td>
	<td><fmt:formatDate pattern="dd/MM/yyyy" 
            value="${taxa.referencia }" /></td>
	<td><fmt:formatNumber value="${taxa.taxa}" 
            type="currency"/></td>
	<td><fmt:formatNumber value="${taxa.multa}" 
            type="currency"/></td>
	<c:choose>
	<c:when test="${taxa.pago == true}">
	<td><span class="label label-success">Sim</span></td>
	</c:when>
	<c:otherwise>
	<td><a href="<c:url value='/CadastrarTaxa.do?ref=receber&id=${taxa.id }'/>"><button class="btn btn-warning" type="button">Receber</button></a></td>
	</c:otherwise>
	</c:choose>
	
<c:choose>
	<c:when test="${taxa.pago == true}">
	<td><fmt:formatDate pattern="dd/MM/yyyy" 
            value="${taxa.data_pagamento }" /></td>
    <td></td>
           
</c:when>
<c:otherwise>
	<td></td>
	 <td><a href="<c:url value='/CadastrarTaxa.do?ref=excluir&id=${taxa.id }'/>"><button class="btn btn-danger" type="button">Excluir</button></a></td>
	
</c:otherwise>
</c:choose>
</tr>

</c:forEach>

</table>
</div>


</body>
</html>