<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" errorPage="Erro.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Reenviar Senha</title>
</head>
<body>
<TagHeader:TagHead/>

<TagHeader:MenuIndex/>


<c:if test="${ requestScope.mensagem != null}">
<div class="alert alert-block">${ requestScope.mensagem}</div>
</c:if>
<form action="enviar.jsp" method="post">
<label for= "email">Email:</label>
<input type="text" name="reenviar"/>
<input type="submit" value="Enviar"/>
</form>

</body>
</html>