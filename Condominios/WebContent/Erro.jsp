<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" isErrorPage="true"%>
  
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Erro</title>
</head>
<body>
<TagHeader:TagHead/>

<TagHeader:MenuIndex/>

<h1>Erro</h1>
<p>Aconteceu um problema, por favor clique em voltar do navegador ou clique<a href="/Condominios/index.jsp">aqui</a>
para retorna ao in�cio.</p>
<h2>Erro:</h2>
<p>${pageContext.exception}
</p>

</body>
</html>