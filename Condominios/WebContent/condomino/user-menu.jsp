<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
</head>

<div class="navbar navbar-inverse">

 <div class="navbar-inner">


<ul class="nav">
		<li>
		<a href="<c:url value='/AdministrarCondomino.do?ref=inicio'/>">In�cio</a>
		</li>
		
		<li>
		<a href="<c:url value='/AdministrarCondomino.do?ref=editarCad'/>">Editar Cadastro</a>
		</li>
		<li><a href="<c:url value='/CadastrarTaxa.do?ref=condomino'/>">Pagamentos</a></li>
		<li><a href="<c:url value='/AdministrarColaborador.do?ref=agendar'/>"> Agendar Atendimento</a></li>
		<li><a href="<c:url value='/AdministrarMensagens.do?ref=mensagens'/>">Mensagens</a></li>
		<li><a href="<c:url value='/Logoff.do'/>"><span class="label label-warning">Fazer Logoff</span></a></li>
</ul>

 <div id="login" style="float: right;">
<c:choose>
 <c:when test="${sessionScope.condomino== null }">
	<c:import url="Login.jsp" />
</c:when>
<c:otherwise>
<div id="welcome">Bem vindo, ${sessionScope.condomino.login }</div>
</c:otherwise>
 
 </c:choose>	
  </div>
</div>
</div>