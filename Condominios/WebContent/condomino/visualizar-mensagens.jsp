<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Mensagens</title>
</head>
<body>
<TagHeader:TagHead/>
<TagHeader:TagSessaoCondomino/>



<c:import url="user-menu.jsp"></c:import>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>
<h5>Voc� est� em : Mensagens</h5>
<table class="table table-striped" >
<tr style="font-weight: bold;">
				<td>#</td>
				<td>Assunto</td>			
				<td>Mensagem</td>			
				<td>Lida</td>
				<td>Excluir</td>
				
</tr>

	<c:forEach var="msg" items="${sessionScope.mensagens }">
		<tr>
		<td>${msg.id }</td>
		<td>${msg.assunto }</td>
		<td>${msg.mensagem }</td>
			<c:choose> <c:when test="${msg.lido == true}">
				<td><span class="label label-success">Lida</span></td>
			</c:when>
			<c:otherwise>
		<td><a href="<c:url value='/AdministrarMensagens.do?ref=ler&id_mensagem=${msg.id }&id_cond=${sessionScope.condomino.id } '/>"><button class="btn btn-info" type="button"> Ler</button></a></td>
		
		</c:otherwise>
		</c:choose>
		<td><a href="<c:url value='/AdministrarMensagens.do?ref=cond_excluir&id_mensagem=${msg.id }&id_cond=${sessionScope.condomino.id } '/>"><button class="btn btn-danger" type="button"> Excluir</button></a></td>
	</tr>
	</c:forEach>

</table>	
	
	
	
	
	
	


</body>
</html>