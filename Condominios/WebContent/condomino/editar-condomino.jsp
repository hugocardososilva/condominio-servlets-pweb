<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" errorPage="Erro.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Editar </title>
</head>
<body>
<TagHeader:TagHead/>
<TagHeader:TagSessaoCondomino/>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>
<h5>Voc� est� em : Editar Perfil</h5>
<c:import url="user-menu.jsp"></c:import>
<form class="navbar-form pull-left" action="CadastrarCondomino.do?ref=update" method="post">
<label for="proprietario">Proprietario</label>
<input type="text" name="proprietario" value="${sessionScope.condomino.proprietario }">

<label for="email">Email:</label>
<input type="text" name="email"  value="${sessionScope.condomino.email }">
<label for="email">CPF:</label>
<input type="text" name="cpf" readonly="readonly"  value="${sessionScope.condomino.cpf }">
<label for="telefone">Telefone:</label>
<input type="text" name="telefone" value="${sessionScope.condomino.telefone }">

<label for="senha">Senha:</label>
<input type="password" name="senha" value="${sessionScope.condomino.senha }">
<label for="repete_senha">Repetir Senha:</label>
<input type="password" name="repete_senha" value="${sessionScope.condomino.senha }">
<br>
<input type="submit" value="Editar">
</form>
</body>
</html>