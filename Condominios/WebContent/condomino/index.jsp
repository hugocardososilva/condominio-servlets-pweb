<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" errorPage="Erro.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Cond�mino</title>
</head>
<body>
<TagHeader:TagHead/>

<TagHeader:TagSessaoCondomino/>






	<c:import url="user-menu.jsp"></c:import>
	
<c:forEach var="mensagens" items="${requestScope.minhalista }">


</c:forEach>

	<c:if test="${sessionScope.listaMensagem !=0 }">
	<div class="alert">
		Voc� tem ${sessionScope.listaMensagem } mensagens n�o lidas, 
		Clique <a href="<c:url value='/AdministrarMensagens.do?ref=mensagens'/>">aqui</a>
		para visualizar.
	</div>
</c:if>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>

<h5>Voc� est� em : In�cio</h5>
<table class="table table-striped">
<tr>
<td>Proprietario: </td>
<td>${sessionScope.condomino.proprietario}</td>

</tr>
<tr>
<td>Apartamento: </td>
<td>${sessionScope.condomino.apartamento}</td>

</tr>
<tr>
<td>Login: </td>
<td>${sessionScope.condomino.login}</td>

</tr>
<tr>
<td>Email: </td>
<td>${sessionScope.condomino.email}</td>

</tr>
<tr>
<td>Cpf: </td>
<td>${sessionScope.condomino.cpf}</td>

</tr>
<tr>
<td>Telefone: </td>
<td>${sessionScope.condomino.telefone}</td>

</tr>
<tr>
<td>Senha: </td>
<td>${sessionScope.condomino.senha}</td>

</tr>
<tr>
<td>Inquilino: </td>
<c:choose><c:when test="${sessionScope.condomino.inquilino == null or sessionScope.condomino.inquilino == '' }">
<td><a href="<c:url value='/CadastrarCondomino.do?ref=inquilino'/>"><button class="btn btn-success" type="button">Cadastrar Inquilino</button></a></td>
</c:when>

<c:otherwise>
<td>${sessionScope.condomino.inquilino}<a href="<c:url value='/CadastrarCondomino.do?ref=removerinquilino'/>" style="float:right;"><button class="btn btn-danger" type="button">Remover Inquilino</button></a></td>
<td></td>
</c:otherwise>
</c:choose>
</tr>
<c:choose><c:when test="${sessionScope.condomino.inquilino == null or sessionScope.condomino.inquilino == '' }">
</c:when>
<c:otherwise>
<tr><td>Moradores:</td>
<td><a href="/Condominios/Moradores.do?ref=morador"><button class="btn btn-primary" type="button">Adicionar morador</button></a></td>
</tr>
<c:choose>	
	<c:when test="${sessionScope.condomino.moradores != null }">
		<tr style="font-weight: bold;">
				<td>Nome:</td>
			
				
			</tr>
		<c:forEach var="morador" items="${sessionScope.condomino.moradores}">
			<tr>
				<td>${morador.nome}</td>
				<td><a href="<c:url value='/Moradores.do?ref=removermorador&morador=${morador.id}'/>"><button class="btn btn-danger" type="button">Remover</button></a></td>
			</tr>
		</c:forEach>
	</c:when>
</c:choose>
<tr><td>Veiculos:</td>
<td><a href="/Condominios/Veiculos.do?ref=veiculo"><button class="btn btn-primary" type="button">Adicionar ve�culo</button></a></td>
</tr>
<c:choose>	
	<c:when test="${sessionScope.condomino.veiculos != null }">
	<tr style="font-weight: bold;">
				<td>Cor</td>
			
				<td>Marca</td>
			
				<td>Placa</td>
			
				<td>Modelo</td>
			</tr>
		<c:forEach var="veiculo" items="${sessionScope.condomino.veiculos}">
			
			<tr>
				<td>${veiculo.cor}</td>
			
				<td>${veiculo.marca}</td>
			
				<td>${veiculo.placa}</td>
			
				<td>${veiculo.modelo}</td>
			</tr>
		</c:forEach>
	</c:when>
</c:choose>
</c:otherwise>
</c:choose>
<tr>




</tr>
</table>



</body>
</html>