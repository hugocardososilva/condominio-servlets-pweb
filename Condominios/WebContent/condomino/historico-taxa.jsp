<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" errorPage="Erro.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Taxas</title>
</head>
<body>
<TagHeader:TagHead/>
<TagHeader:TagSessaoCondomino/>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>
<c:import url="user-menu.jsp"></c:import>
<h5>Voc� est� em : Taxas e pagamentos</h5>

<table  class="table table-striped">
<tr>
	
	<td>Referencia</td>
	<td>Valor</td>
	<td>Multa</td>
	<td>Pago ?</td>
	
</tr>
<c:forEach var="taxa" items="${requestScope.taxas }">

<c:choose>
<c:when test="${taxa.condomino.id == sessionScope.condomino.id }">
	<tr>
	
	<td><fmt:formatDate pattern="dd/MM/yyyy" 
            value="${taxa.referencia }" /></td>
	<td><fmt:formatNumber value="${taxa.taxa}" 
            type="currency"/></td>
	<td><fmt:formatNumber value="${taxa.multa}" 
            type="currency"/></td>
	<c:choose>
	<c:when test="${taxa.pago == true}">
	<td>Sim</td>
	</c:when>
	<c:otherwise>
	<td><a href="<c:url value='/CadastrarTaxa.do?ref=boleto&id=${taxa.id }'/>"><button class="btn btn-success" type="button">Gerar Boleto</button></a></td>
	</c:otherwise>
	</c:choose>
</c:when>
</c:choose>
</c:forEach>
</table>
</body>
</html>