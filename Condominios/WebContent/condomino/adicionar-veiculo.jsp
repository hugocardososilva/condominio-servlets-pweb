<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" errorPage="Erro.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Adicionar Veiculo</title>
</head>
<body>
<TagHeader:TagHead/>
<TagHeader:TagSessaoCondomino/>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>
<h5>Voc� est� em : Adicionar Veiculo</h5>
<c:import url="user-menu.jsp"></c:import>
<form action="Veiculos.do?ref=add" method="post">
Cor:<input type="text" name="cor"><br>
Marca:<input type="text" name="marca"><br>
Placa:<input type="text" name="placa"><br>
Modelo:<input type="text" name="modelo"><br>
<input type="submit" value="Cadastrar">
</form>

</body>
</html>