<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" errorPage="Erro.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="../style.css">
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="../bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Agendar Atendimento</title>
</head>
<body>
<body>
<TagHeader:TagHead/>
<TagHeader:TagSessaoCondomino/>
<Mensage:mensagens mensagem="${requestScope.mensagem }"/>
<h5>Voc� est� em : Agendar Atendimento</h5>  
<c:import url="user-menu.jsp"></c:import>

<form action="AdministrarHorarios.do?ref=agendarHorario" method="post">
<select name="colaborador" size="4">
<c:forEach var="colaborador" items="${requestScope.colaborador }">
Colaborador: <option  value="${colaborador.id }">${colaborador.nome }</option>
</c:forEach>
</select><br>
Data: <input type="text" name="dia" size="2" >/ <input type="text" name="mes"size="2" > /<input type="text" name="ano"size="4" >
 Hora: <input type="text" name="hora" size="2" >: <input type="text" name="minuto"size="2" >
<input type="submit" value="Cadastrar">



</form>
<table class="table table-striped">
<thead><tr>Atendimentos agendados</tr></thead>
<tbody>
	<tr>
		<td>ID</td>
		<td>Data</td>
		<td>Colaborador</td>
	</tr>
<c:forEach items="${sessionScope.horarios }" var="horarios">
	<tr>
		<td>${horarios.id }</td>
		<td>${horarios.data }</td>
		<td>${horarios.colaborador.nome }</td>
	</tr>
</c:forEach>
</tbody>


</table>
</body>
</html>