<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" errorPage="Erro.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Sistema de Condom�nio</title>
</head>
<body>
<TagHeader:TagHead/>

<TagHeader:MenuIndex/>

<Mensage:mensagens mensagem="${requestScope.mensagem }"/>


<p></p>

<h2>Condom�o IFPB</h2>
<p>Bem vindo, se voc� � cond�mino do IFPB e n�o tem cadastro, por favor efetue o cadastro no link ao lado.</p>
<p>Caso j� tenha cadastro, por favor efetue o login no formul�rio acima. <p>
<p>Esqueceu sua senha? Clique <a href="<c:url value='reenviar-senha.jsp'/>">aqui</a></p>
<TagHeader:TagFooter></TagHeader:TagFooter>

</body>

</html>