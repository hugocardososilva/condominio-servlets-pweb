<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<link rel="stylesheet" type="text/css" href="style.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>N�o autorizado</title>
</head>
<body>
<TagHeader:TagHead/>

<TagHeader:MenuIndex/>

<c:import url="header.jsp" />
<p>Voc� nao tem autoriza��o para acessar essa p�gina, retorne a  <a href="/Condominios/index.jsp">p�gina inicial</a></p>
</body>
</html>