<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="Mensage" uri="condomino.tags"%>
<%@ taglib prefix="TagHeader" tagdir="/WEB-INF/tags"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1" errorPage="Erro.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link rel="stylesheet" type="text/css" href="style.css">

<link rel="stylesheet" type="text/css" href="bootstrap-default.css">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">

<title>Formulário de Cadastro</title>
</head>
<body>
<TagHeader:TagHead/>

<TagHeader:MenuIndex/>


<p>${sessionScope.mensagem }</p>

<form action="CadastrarCondomino.do?ref=add" method="post">
<table>
<tr>
<td>
Proprietario:</td>
<td>
<input type="text" name="proprietario" value="${sessionScope.proprietario }" ><br></td>
</tr>
<tr>
<td>
CPF:</td>
<td>
<input type="text" name=cpf value="${sessionScope.cpf }" ><br></td>
</tr>
<tr>
<td>
Numero do Apartamento:</td>
<td>
<input type="text" name="numeroAp"value="${sessionScope.numeroAp }" ><br></td></tr>
<tr>
<td>
Email:</td>
<td>
<input type="text" name="email" value="${sessionScope.email }"><br></td></tr>
<tr>
<td>
Telefone:</td>
<td>
<input type="text" name="telefone"value="${sessionScope.telefone }" ><br></td></tr>

<tr>
<td>
<input type="submit" value="Enviar"></td>
</tr>
</table>
</form>

</body>
</html>