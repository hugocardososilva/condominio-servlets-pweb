package model;

import java.util.Date;

public class Horarios {
	private int id;
	private Date data;
	Condomino condomino;
	Colaborador colaborador;
	

	public Horarios() {
		super();
	}
	
	public Colaborador getColaborador() {
		return colaborador;
	}

	public void setColaborador(Colaborador colaborador) {
		this.colaborador = colaborador;
	}

	public Condomino getCondomino() {
		return condomino;
	}

	public void setCondomino(Condomino condomino) {
		this.condomino = condomino;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	@Override
	public String toString() {
		return "Horarios [id=" + id + ", data=" + data + ", condomino="
				+ condomino + "]";
	}



	
}
