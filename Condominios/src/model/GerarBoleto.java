package model;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;

import org.eclipse.jdt.internal.compiler.lookup.MostSpecificExceptionMethodBinding;
import org.jrimum.bopepo.BancosSuportados;
import org.jrimum.bopepo.Boleto;
import org.jrimum.bopepo.view.BoletoViewer;
import org.jrimum.domkee.financeiro.banco.febraban.Agencia;
import org.jrimum.domkee.financeiro.banco.febraban.Carteira;
import org.jrimum.domkee.financeiro.banco.febraban.Cedente;
import org.jrimum.domkee.financeiro.banco.febraban.ContaBancaria;
import org.jrimum.domkee.financeiro.banco.febraban.NumeroDaConta;
import org.jrimum.domkee.financeiro.banco.febraban.Sacado;
import org.jrimum.domkee.financeiro.banco.febraban.TipoDeTitulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo;
import org.jrimum.domkee.financeiro.banco.febraban.Titulo.Aceite;

import model.Condomino;
import model.Taxa;
import DAO.CondominoDAO;

public class GerarBoleto {
	
	
	private Taxa taxa;

	public GerarBoleto(Taxa taxa) {
		super();
		this.taxa = taxa;
	}

	public final Taxa getTaxa() {
		return taxa;
	}

	public final void setTaxa(Taxa taxa) {
		this.taxa = taxa;
	}
	
	public void gerarBoleto(){
		StringBuffer numDoc= new StringBuffer();
		numDoc.append(this.taxa.getCondomino().getCpf());
		numDoc.append(this.taxa.getId());
		
		Cedente cedente = new Cedente("Hugo Cardoso", "051.893.014-94");
		Sacado sacado = new Sacado(this.taxa.getCondomino().getProprietario()); 
		ContaBancaria conta= new ContaBancaria(BancosSuportados.BANCO_DO_BRASIL.create());
		conta.setAgencia(new Agencia(12345));
		conta.setCarteira(new Carteira(30));
		conta.setNumeroDaConta(new NumeroDaConta(1234, "1"));
		
		Titulo titulo = new Titulo(conta, sacado, cedente);
		titulo.setNumeroDoDocumento(numDoc.toString());
		//titulo.setNumeroDoDocumento("123455");
		titulo.setNossoNumero("1212121212");
		titulo.setValor(BigDecimal.valueOf(this.taxa.getTaxa()));
		
		titulo.setDataDoDocumento(this.taxa.getReferencia());
		titulo.setDataDoVencimento(this.taxa.getReferencia());
		titulo.setTipoDeDocumento(TipoDeTitulo.DM_DUPLICATA_MERCANTIL);
		titulo.setAceite(Aceite.A);
		titulo.setMora(BigDecimal.valueOf(this.taxa.getMulta()));
		titulo.setValorCobrado(BigDecimal.valueOf(this.taxa.getTaxa()+this.taxa.getPagarMulta()));
		
		Boleto boleto = new Boleto(titulo);
        
        boleto.setLocalPagamento("Pag�vel preferencialmente na Rede X ou em " +
                        "qualquer Banco at� o Vencimento.");
        boleto.setInstrucao1("O valor da multa di�ria � de R$ "+ this.taxa.getMulta()+ " a partir do vencimento");
        boleto.setInstrucao2("IFPB");
        BoletoViewer boletoViewer = new BoletoViewer(boleto);
		
        File arquivoPdf = boletoViewer.getPdfAsFile(numDoc+".pdf");
        mostreBoletoNaTela(arquivoPdf);
	}
	 private static void mostreBoletoNaTela(File arquivoBoleto) {

         java.awt.Desktop desktop = java.awt.Desktop.getDesktop();
         
         try {
                 desktop.open(arquivoBoleto);
         } catch (IOException e) {
                 e.printStackTrace();
         }
 }
}

