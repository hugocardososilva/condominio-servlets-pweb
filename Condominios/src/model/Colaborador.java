package model;


import java.util.Date;
import java.util.List;

/**
 * @author the_s_000
 *
 */
public class Colaborador {
	private int id;
	private long telefone, cpf;
	private String nome, servico;
	private List<Horarios> horarios;
//	public Colaborador(int telefone, int cpf, String nome, String servico) {
//		super();
//		this.telefone = telefone;
//		this.cpf = cpf;
//		this.nome = nome;
//		this.servico = servico;
//		this.horarios = new ArrayList<Date>();
//	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public long getTelefone() {
		return telefone;
	}
	public void setTelefone(long telefone) {
		this.telefone = telefone;
	}
	public long getCpf() {
		return cpf;
	}
	public void setCpf(long cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getServico() {
		return servico;
	}
	public void setServico(String servico) {
		this.servico = servico;
	}
	public List<Horarios> getHorarios() {
		return horarios;
	}
	public void setHorarios(List<Horarios> horarios) {
		this.horarios = horarios;
	}
	@Override
	public String toString() {
		return "Colaborador [id=" + id + ", telefone=" + telefone + ", cpf="
				+ cpf + ", nome=" + nome + ", servico=" + servico
				+ ", horarios=" + horarios + "]";
	}
	
}
	
