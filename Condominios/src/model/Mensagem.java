package model;

import java.util.ArrayList;

public class Mensagem {
	private String titulo, assunto, mensagem;
	private int id;
	private ArrayList<Condomino> destinatarios;
	private boolean lido;
	
	public Mensagem() {
		super();
		// TODO Auto-generated constructor stub
	}

	public String getTitulo() {
		return titulo;
	}

	public String getMensagem() {
		return mensagem;
	}

	public void setMensagem(String mensagem) {
		this.mensagem = mensagem;
	}

	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public ArrayList<Condomino> getDestinatarios() {
		return destinatarios;
	}

	public void setDestinatarios(ArrayList<Condomino> destinatarios) {
		this.destinatarios = destinatarios;
	}
	public void addDestinatarios(Condomino c) {
		this.destinatarios.add(c);
		
	}
	public void removeDestinatarios(Condomino c){
		this.destinatarios.remove(c);
	}
	
	public boolean isLido() {
		return lido;
	}

	public void setLido(boolean lido) {
		this.lido = lido;
	}

	@Override
	public String toString() {
		return "Mensagem [titulo=" + titulo + ", assunto=" + assunto
				+ ", mensagem=" + mensagem + ", id=" + id + ", destinatarios="
				+ destinatarios + ", lido=" + lido + "]";
	}

	

}
