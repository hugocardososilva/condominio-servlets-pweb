package model;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;

import com.itextpdf.text.pdf.hyphenation.TernaryTree.Iterator;

import com.sun.xml.internal.ws.policy.privateutil.PolicyUtils.Collections;

import DAO.CondominoDAO;
import DAO.TaxaDAO;

public class Painel {
	//private List<Condomino> condomino;
	private List<Taxa> taxa;
	private HashMap<Integer, Taxa> taxas;
	private Float atraso, receita;
	private Condomino condomino;
	private int ano;
	
	public Painel(Condomino condomino, int ano) {
		super();
		this.condomino = condomino;
		this.ano= ano;
		this.taxa = new ArrayList<Taxa>();
		this.taxas= new HashMap<Integer, Taxa>();
		this.atraso = 0.00f;
		this.receita= 0.00f;
	}
//	public List<Condomino> getCondomino() throws Exception {
//		CondominoDAO dao = new CondominoDAO();
//		
//		return dao.getAll();
//	}
//	
	
	public List<Taxa> getTaxa() throws Exception {
		TaxaDAO dao = new TaxaDAO();
		//this.taxa = dao.getAnoByCondomino(this.getCondomino(),this.getAno());
		
		for (Taxa t : dao.getAnoByCondomino(this.getCondomino(), this.getAno())){
			Calendar calendar = Calendar.getInstance();
				if ( calendar.get(Calendar.YEAR)== this.getAno()){
					this.taxa.add(t);
				}
		}
		
		//System.out.println("minhas taxas" + taxas.toString());
		
		return taxa;
	}
	public final Float getReceita() {
		return receita;
	}

	public final void setReceita(Float receita) {
		this.receita = receita;
	}

	public Condomino getCondomino() {
		return condomino;
	}

	public void setCondomino(Condomino condomino) {
		this.condomino = condomino;
	}

	public void setTaxas(List<Taxa> taxa) {
		this.taxa = taxa;
	}
	public Float getAtraso() {
		return atraso;
	}
	public void setAtraso(Float atraso) {
		this.atraso = atraso;
	}

	public int getAno() {
		return ano;
	}
	

	public HashMap<Integer, Taxa> getTaxas() {
		try {
			return this.bucarTaxaMes();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return taxas;
	}

	public void setTaxxas(HashMap<Integer, Taxa> taxa_mes) {
		this.taxas = taxa_mes;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}
	
	public Float calcularAtraso() throws Exception{
		for (Taxa t : (ArrayList<Taxa>) this.getTaxa()){
			System.out.println(t.getTaxa());
			if(!t.isPago()){
				this.atraso = this.atraso + t.getTaxa();
				
			}
		}
		
		return this.atraso;
	}
	public HashMap<Integer, Taxa> bucarTaxaMes() throws Exception{
		taxas.put(1, null);
		taxas.put(2, null);
		taxas.put(3, null);
		taxas.put(4, null);
		taxas.put(5, null);
		taxas.put(6, null);
		taxas.put(7, null);
		taxas.put(8, null);
		taxas.put(9, null);
		taxas.put(10, null);
		taxas.put(11, null);
		taxas.put(12, null);
		for(Taxa t: this.getTaxa()){
			Calendar data = Calendar.getInstance();
			data.setTime(t.getReferencia());
			
			if(data.get(Calendar.MONTH) == Calendar.JANUARY){
				taxas.put(1, t);
				
			}
				if(data.get(Calendar.MONTH) == Calendar.FEBRUARY){
					taxas.put(2, t);
					
			}
				if(data.get(Calendar.MONTH)== Calendar.MARCH){
					taxas.put(3, t);
					
			}
				if(data.get(Calendar.MONTH) == Calendar.APRIL){
					taxas.put(4, t);
					
			}
				if(data.get(Calendar.MONTH) == Calendar.MAY){
					taxas.put(5, t);
					
			}
				if(data.get(Calendar.MONTH) == Calendar.JUNE){
					taxas.put(6, t);
					
			}
				if(data.get(Calendar.MONTH) == Calendar.JULY){
					taxas.put(7, t);
					
			}
				if(data.get(Calendar.MONTH) == Calendar.AUGUST){
					taxas.put(8, t);
					
			}
				if(data.get(Calendar.MONTH) == Calendar.SEPTEMBER){
					taxas.put(9, t);
					
			}
				if(data.get(Calendar.MONTH) == Calendar.OCTOBER){
					taxas.put(10, t);
					
			}
				if(data.get(Calendar.MONTH) == Calendar.NOVEMBER){
					taxas.put(11, t);
					
			}
				if(data.get(Calendar.MONTH) == Calendar.DECEMBER){
					taxas.put(12, t);
						
				}
				
		}
		System.out.println("retornando taxa  " +taxas.toString());
		
		return taxas;
	}
				
			
		
	public Float calcularReceita() throws Exception{
		for(Taxa t: this.getTaxa()){
			if(t.isPago()){
			this.receita= this.receita+ t.getTaxa();
			}
		}
		return this.receita;
	}


}