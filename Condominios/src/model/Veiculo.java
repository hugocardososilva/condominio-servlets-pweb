
package model;

public class Veiculo {
	private int id;
	private String cor, marca, placa, modelo;
	
	
	public Veiculo(int id, String cor, String marca, String placa, String modelo) {
		super();
		this.id = id;
		this.cor = cor;
		this.marca = marca;
		this.placa = placa;
		this.modelo = modelo;
	}
	public Veiculo(){
		super();
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCor() {
		return cor;
	}
	public void setCor(String cor) {
		this.cor = cor;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String marca) {
		this.marca = marca;
	}
	public String getPlaca() {
		return placa;
	}
	public void setPlaca(String placa) {
		this.placa = placa;
	}
	public String getModelo() {
		return modelo;
	}
	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	@Override
	public String toString() {
		return "Veiculo [id=" + id + ", cor=" + cor + ", marca=" + marca
				+ ", placa=" + placa + ", modelo=" + modelo + "]";
	}
	

}
