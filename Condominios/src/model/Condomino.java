package model;

import java.util.ArrayList;
import java.util.List;

public class Condomino {
	private int id, apartamento	;
	private long telefone, cpf;
	private String proprietario,email,inquilino, senha, login;
	private boolean alugado, revisado, novo;
	private List<Morador> moradores;
	private List<Veiculo> veiculos;
	
//	public Condomino(int telefone, int apartamento, String proprietario,
//			String email,String senha, boolean alugado,
//			boolean revisado) {
//		super();
//		this.telefone = telefone;
//		this.apartamento = apartamento;
//		this.proprietario = proprietario;
//		this.email = email;
//		
//		this.senha = senha;
//		this.alugado = false;
//		this.revisado = revisado;
//		this.moradores= new ArrayList<>();
//		this.veiculos = new ArrayList<Veiculo>();
//	}
	
	
	public Condomino() {
		super();
	}


	


	


	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public boolean isNovo() {
		return novo;
	}


	public void setNovo(boolean novo) {
		this.novo = novo;
	}


	public int getId() {
		return id;
	}
	public void setId(int id){
		this.id= id;
	}
	
	
	public final long getTelefone() {
		return telefone;
	}








	public final void setTelefone(long telefone) {
		this.telefone = telefone;
	}








	public final long getCpf() {
		return cpf;
	}








	public final void setCpf(long cpf) {
		this.cpf = cpf;
	}








	public int getApartamento() {
		return apartamento;
	}
	public void setApartamento(int apartamento) {
		this.apartamento = apartamento;
	}
	public String getProprietario() {
		return proprietario;
	}
	public void setProprietario(String proprietario) {
		this.proprietario = proprietario;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getInquilino() {
		return inquilino;
	}
	public void setInquilino(String inquilino) {
		this.inquilino = inquilino;
	}
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public boolean isAlugado() {
		return alugado;
	}
	public void setAlugado(boolean alugado) {
		this.alugado = alugado;
	}
	public boolean isRevisado() {
		return revisado;
	}
	public void setRevisado(boolean revisado) {
		this.revisado = revisado;
	}
	public List<Morador> getMoradores() {
		return moradores;
	}
	public void setMoradores(List<Morador> moradores) {
		this.moradores = moradores;
	}
	public List<Veiculo> getVeiculos() {
		return veiculos;
	}
	public void setVeiculos(List<Veiculo> veiculos) {
		this.veiculos = veiculos;
	}
	public void addVeiculos(Veiculo veiculo){
		this.veiculos.add(veiculo);
	}
	public void removeVeiculo(Veiculo veiculo){
		this.veiculos.remove(veiculo);
	}








	@Override
	public String toString() {
		return "Condomino [id=" + id + ", apartamento=" + apartamento
				+ ", telefone=" + telefone + ", cpf=" + cpf + ", proprietario="
				+ proprietario + ", email=" + email + ", inquilino="
				+ inquilino + ", senha=" + senha + ", login=" + login
				+ ", alugado=" + alugado + ", revisado=" + revisado + ", novo="
				+ novo + ", moradores=" + moradores + ", veiculos=" + veiculos
				+ "]";
	}


	

	
	
	

}
