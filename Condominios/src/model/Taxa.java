package model;

import java.util.Date;

public class Taxa {
	private int id;
	private Condomino condomino;
	private Date referencia, data_pagamento;
	private Float taxa, multa, pagarMulta;
	private boolean pago;
	
	public Taxa(Condomino condomino, Date referencia, Float taxa, Float multa) {
		super();
		this.condomino = condomino;
		this.referencia = referencia;
		this.taxa = taxa;
		this.multa = multa;
		this.pago= false;
	}

	public Date getData_pagamento() {
		return data_pagamento;
	}

	public void setData_pagamento(Date data_pagamento) {
		this.data_pagamento = data_pagamento;
	}

	public Float getPagarMulta() {
		return pagarMulta;
	}

	public void setPagarMulta(Float pagarMulta) {
		this.pagarMulta = pagarMulta;
	}

	public void setTaxa(Float taxa) {
		this.taxa = taxa;
	}

	public void setMulta(Float multa) {
		this.multa = multa;
	}

	public Taxa() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Condomino getCondomino() {
		return condomino;
	}

	public void setCondomino(Condomino condomino) {
		this.condomino = condomino;
	}

	public Date getReferencia() {
		return referencia;
	}

	public void setReferencia(Date referencia) {
		this.referencia = referencia;
	}

	public Float getTaxa() {
		return taxa;
	}

	public Float getMulta() {
		return multa;
	}
	

	public boolean isPago() {
		return pago;
	}

	public void setPago(boolean pago) {
		this.pago = pago;
	}

	@Override
	public String toString() {
		return "Taxa [id=" + id + ", condomino=" + condomino + ", referencia="
				+ referencia + ", data_pagamento=" + data_pagamento + ", taxa="
				+ taxa + ", multa=" + multa + ", pagarMulta=" + pagarMulta
				+ ", pago=" + pago + "]";
	}
	
	
	

}
