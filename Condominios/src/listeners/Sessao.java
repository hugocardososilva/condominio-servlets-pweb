package listeners;

import javax.servlet.ServletRequest;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

/**
 * Application Lifecycle Listener implementation class Sessao
 *
 */
@WebListener
public class Sessao implements HttpSessionListener {

    /**
     * Default constructor. 
     */
    public Sessao() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see HttpSessionListener#sessionCreated(HttpSessionEvent)
     */
    public void sessionCreated(HttpSessionEvent e) {
    	System.out.println("Sessao criada : " + e.getSession().getId());
    }

	/**
     * @see HttpSessionListener#sessionDestroyed(HttpSessionEvent)
     */
    public void sessionDestroyed(HttpSessionEvent e) {
       System.out.println("Sessao destruida : " + e.getSession().getId());
       
    }
	
}
