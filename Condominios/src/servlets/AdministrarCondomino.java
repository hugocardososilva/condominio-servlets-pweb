package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import email.EnviarEmail;

import DAO.CondominoDAO;
import DAO.MensagensDAO;

import model.Condomino;
import model.Mensagem;

/**
 * Servlet implementation class AdministrarCondomino
 */
@WebServlet("/AdministrarCondomino.do")
public class AdministrarCondomino extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
   
    public AdministrarCondomino() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ref= request.getParameter("ref");
		CondominoDAO dao= new CondominoDAO();
		List<Condomino> lista= new ArrayList<Condomino>();
		
		try {
			lista= dao.getAll();
			for(Condomino c: lista){
				c.setVeiculos(dao.getVeiculos(c));
				c.setMoradores(dao.getMoradores(c));
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
			
		if(ref.equalsIgnoreCase("listar")){
			
			
				request.setAttribute("lista", lista);
				System.out.println(lista.toString());
				request.getRequestDispatcher("admin/listar-condominos.jsp").forward(request, response);
				//response.sendRedirect("admin/listar-condominos.jsp");
			
		}else 
			if(ref.equalsIgnoreCase("aprovar")){
				int id= Integer.parseInt(request.getParameter("id"));
				
				Condomino c = new Condomino();
				
				try {
					c= dao.getById(id);
					c.setRevisado(true);
					dao.update(c);
					lista= null;
					lista= dao.getAll();
					for(Condomino co: lista){
						co.setVeiculos(dao.getVeiculos(co));
						co.setMoradores(dao.getMoradores(co));
					}
					StringBuffer sb= new StringBuffer();
					sb.append("Ola, bem vindo ao sistema de condominio\n");
					sb.append("Seu login:\n");
					sb.append(c.getLogin()+" \n");
					sb.append("sua senha: ");
					sb.append(c.getSenha() + "\n");
					sb.append("Proprietario: " + c.getProprietario());
					sb.append("\nEmail: " + c.getEmail());
					sb.append("\n Telefone: " + c.getTelefone());
					sb.append("\n n Clique no link para ir a p�gina http://localhost:8082/Condominio");
					String assunto = sb.toString();
					String titulo = "Cadastro de Cond�mino";
					EnviarEmail enviarEmail= new EnviarEmail();
					ServletContext sc = getServletContext();
					String mensagem= enviarEmail.Enviar(c.getEmail(), assunto, titulo, sc);
					
										
				
				request.setAttribute("lista", lista);
				request.setAttribute("mensagem", mensagem);
				System.out.println(ref + c.toString());
				//response.sendRedirect("admin/listar-condominos.jsp");
				request.getRequestDispatcher("admin/listar-condominos.jsp").forward(request, response);
				} catch (Exception e) {
					// TODO: handle exception
				}
			}else 
				if(ref.equalsIgnoreCase("editarCad")){
					request.getRequestDispatcher("condomino/editar-condomino.jsp").forward(request, response);
				
			}else
				if(ref.equalsIgnoreCase("inicio")){
					request.getRequestDispatcher("condomino/index.jsp").forward(request, response);
				}
				
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

}
