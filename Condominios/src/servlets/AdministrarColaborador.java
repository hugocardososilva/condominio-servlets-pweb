package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Colaborador;

import DAO.ColaboradorDAO;

/**
 * Servlet implementation class AdministrarColaborador
 */
@WebServlet("/AdministrarColaborador.do")
public class AdministrarColaborador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdministrarColaborador() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ref = request.getParameter("ref");
		ColaboradorDAO dao = new ColaboradorDAO();
		List<Colaborador> lista = new ArrayList<Colaborador>();
		
		
		if (ref.equalsIgnoreCase("listar")){
			try {
				lista= dao.getAll();
				request.setAttribute("colaboradores", lista);
				request.getRequestDispatcher("admin/listar-colaboradores.jsp").forward(request, response);
				
			} catch (Exception e) {
				// TODO: handle exception
			}
			
				
		}
		else
			if(ref.equalsIgnoreCase("editar")){
				int id = Integer.parseInt(request.getParameter("id"));
				try {
					Colaborador c = new Colaborador();
					c= dao.getById(id);
					System.out.println(c.toString());
					request.setAttribute("colaborador", c);
					request.getRequestDispatcher("admin/editar-colaboradores.jsp").forward(request, response);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
				
			}
			else
				if(ref.equalsIgnoreCase("remover")){
					int id = Integer.parseInt(request.getParameter("id"));
					Colaborador c= new Colaborador();
					try {
						
						c=dao.getById(id);
						System.out.println(c);
						dao.remove(c);
						request.setAttribute("colaboradores", dao.getAll());
						request.getRequestDispatcher("admin/listar-colaboradores.jsp").forward(request, response);
						
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				else
					if(ref.equalsIgnoreCase("gerenciar")){
						int id = Integer.parseInt(request.getParameter("id"));
						
						try {
							Colaborador c = new Colaborador();
							c=dao.getById(id);
							c.setHorarios(dao.getHorarios(c));
							request.setAttribute("colaborador", c);
							System.out.println(id + c.toString());
							request.getRequestDispatcher("admin/gerenciar-colaborador.jsp").forward(request, response);
							
							
							
						} catch (Exception e) {
							// TODO: handle exception
						}
					}else
						if(ref.equalsIgnoreCase("agendar")){
							List<Colaborador> colaboradores= new ArrayList<Colaborador>();
							try {
								colaboradores= dao.getAll();
								request.setAttribute("colaborador", colaboradores);
							
							
							request.getRequestDispatcher("condomino/agendar-atendimento.jsp").forward(request, response);
							}catch(Exception e){
								e.printStackTrace();
							}
							}
							
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String ref= request.getParameter("ref");
		ColaboradorDAO dao = new ColaboradorDAO();
		
			if (ref.equalsIgnoreCase("editarCol")){
				int id = Integer.parseInt(request.getParameter("id"));
				String nome = request.getParameter("nome");
				long cpf= Long.parseLong(request.getParameter("cpf"));
				long telefone= Long.parseLong(request.getParameter("telefone"));
				String servico= request.getParameter("servico");
				
				Colaborador c= new Colaborador();
				c.setId(id);
				c.setCpf(cpf);
				c.setNome(nome);
				c.setServico(servico);
				c.setTelefone(telefone);
				
				try {
					dao.update(c);
					request.setAttribute("colaboradores", dao.getAll());
					request.setAttribute("mensagem", "Colaborador " + c.getId() + " atualizado com sucesso");
					request.getRequestDispatcher("admin/listar-colaboradores.jsp").forward(request, response);
					
				} catch (Exception e) {
					System.out.println(e.toString());
				}
				
			}
	}

}
