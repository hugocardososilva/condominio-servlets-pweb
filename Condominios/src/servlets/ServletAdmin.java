package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.AdministradorDAO;
import DAO.CondominoDAO;

import model.Administrador;
import model.Condomino;
import model.Painel;

/**
 * Servlet implementation class ServletAdmin
 */
@WebServlet("/ServletAdmin.do")
public class ServletAdmin extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ServletAdmin() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ref = request.getParameter("ref");
		if(ref.equalsIgnoreCase("painel")){
			String ano= request.getParameter("ano");
			ArrayList<Painel> paineis= new ArrayList<Painel>();
			CondominoDAO condao= new CondominoDAO();
			Float total_atraso = 0f;
			Float total_receita= 0f;
			
			try {
				for(Condomino c : condao.getAll()){
					if(c.isRevisado()){
					Painel painel = new Painel(c, Integer.parseInt(ano));
					total_atraso = total_atraso + painel.calcularAtraso();
					total_receita= total_receita+ painel.calcularReceita();
					System.out.println(painel.getTaxas().values().toString());
					
					paineis.add(painel);
					}
				}
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("paineis", paineis);
			request.setAttribute("total_atrasado", total_atraso);
			request.setAttribute("total_receita", total_receita);
			request.getRequestDispatcher("admin/painel.jsp").forward(request, response);
			
		}else
		if(ref.equalsIgnoreCase("inadiplencia")){
			request.getRequestDispatcher("admin/painel.jsp").forward(request, response);
		}else
			if(ref.equalsIgnoreCase("inicio")){
				request.getRequestDispatcher("admin/index.jsp").forward(request, response);
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
			response.setContentType("text/html");
			String ref = request.getParameter("ref");
			HttpSession session = request.getSession();
				if(ref.equalsIgnoreCase("login")){
					String email= request.getParameter("email");
					String senha = request.getParameter("senha");
					Administrador a = new Administrador();
					AdministradorDAO dao = new AdministradorDAO();
					CondominoDAO daocon= new CondominoDAO();
					try {
						
					
					a= dao.login(email, senha);
						if(a!=null){
							session.setAttribute("condominos", daocon.getAll());
							session.setAttribute("admin", a);
							request.setAttribute("mensagem", "login efetuado com sussesso");
							request.getRequestDispatcher("admin/index.jsp").forward(request, response);
					
					}else{
							request.setAttribute("mensagem", "login ou senha incorretos");
							request.getRequestDispatcher("admin/login-redirect.jsp").forward(request, response);
					}
					} catch (Exception e) {
						System.out.println(e);
					}
					
				}
			
	}

}
