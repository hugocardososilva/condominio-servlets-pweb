package servlets;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.CondominoDAO;

import model.Condomino;
import model.Veiculo;

/**
 * Servlet implementation class Veiculos
 */
@WebServlet("/Veiculos.do")
public class Veiculos extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Veiculos() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ref = request.getParameter("ref");
		if (ref.equalsIgnoreCase("veiculo")) request.getRequestDispatcher("condomino/adicionar-veiculo.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		HttpSession session = request.getSession();
		String ref = request.getParameter("ref");
		
		
		
		if(ref.equalsIgnoreCase("add")){
			
			Condomino c = (Condomino) session.getAttribute("condomino");
			Veiculo v= new Veiculo();
			v.setCor(request.getParameter("cor"));
			v.setMarca(request.getParameter("marca"));
			v.setPlaca(request.getParameter("placa"));
			v.setModelo(request.getParameter("modelo"));
			CondominoDAO dao = new CondominoDAO();
			
			
				try {
					dao.addVeiculo(c, v);
					c.setVeiculos(null);
					c.setVeiculos(dao.getVeiculos(c));
					session.setAttribute("condomino", c);
					
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println(e.toString());
				}
			
			//request.getRequestDispatcher("condomino/index.jsp").forward(request, response);
				response.sendRedirect("condomino/index.jsp");
		}
	}

}
