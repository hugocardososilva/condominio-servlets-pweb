package servlets;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Colaborador;
import model.Condomino;
import model.Horarios;

import DAO.ColaboradorDAO;
import DAO.CondominoDAO;

/**
 * Servlet implementation class AdministrarHorarios
 */
@WebServlet("/AdministrarHorarios.do")
public class AdministrarHorarios extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdministrarHorarios() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ref = request.getParameter("ref");
		ColaboradorDAO dao = new ColaboradorDAO();
		CondominoDAO cdao= new CondominoDAO();
			
			if(ref.equalsIgnoreCase("add")){
				int id = Integer.parseInt(request.getParameter("id"));
				Colaborador c = new Colaborador();
				List<Condomino> lista= new ArrayList<Condomino>();
				try {
					
				lista= cdao.getAll();
				c= dao.getById(id);
				c.setHorarios(dao.getHorarios(c));
				request.setAttribute("colaborador", c);
				request.setAttribute("condominos", lista);
				request.getRequestDispatcher("admin/agendar-atendimento.jsp").forward(request, response);
				
				} catch (Exception e) {
					// TODO: handle exception
				}
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ref = request.getParameter("ref");
			
			if(ref.equalsIgnoreCase("add")){
				String condomino= request.getParameter("condomino");
				String idColaborador= request.getParameter("id");
				String dia = request.getParameter("dia");
				String mes = request.getParameter("mes");
				String ano = request.getParameter("ano");
				String hora = request.getParameter("hora");
				String minuto = request.getParameter("minuto");
				ColaboradorDAO dao= new ColaboradorDAO();
				CondominoDAO cdao= new CondominoDAO();
				Condomino cond= new Condomino();
				Colaborador col= new Colaborador();
				Horarios h = new Horarios();
				SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy HH:mm");
				
					String data = dia+"/"+mes+"/"+ano+" "+hora+":"+minuto+"  ";
					Date date;
					List<Horarios> lista= new ArrayList<Horarios>();
					try {
						date = (Date) formatoData.parse(data);
						
						h.setData(date);
						cond= cdao.getById(Integer.parseInt(condomino));
						col= dao.getById(Integer.parseInt(idColaborador));
						dao.addAtendimento(col, h, cond);
						lista=null;
						lista= dao.getHorarios(col);
						
						col.setHorarios(lista);
						System.out.println(h.getData());
						request.setAttribute("colaborador", col);
						request.getRequestDispatcher("admin/gerenciar-colaborador.jsp").forward(request, response);
						
						
						
						
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					
				
				
			}else
				if(ref.equalsIgnoreCase("agendarHorario")){
					HttpSession session= request.getSession();
					Condomino c = new Condomino();
					c=(Condomino) session.getAttribute("condomino");
					String idColaborador= request.getParameter("colaborador");
					String dia = request.getParameter("dia");
					String mes = request.getParameter("mes");
					String ano = request.getParameter("ano");
					String hora = request.getParameter("hora");
					String minuto = request.getParameter("minuto");
					ColaboradorDAO dao= new ColaboradorDAO();
					
					
					Colaborador col= new Colaborador();
					Horarios h = new Horarios();
					SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy HH:mm");
					
						String data = dia+"/"+mes+"/"+ano+" "+hora+":"+minuto+"  ";
						Date date;
						List<Horarios> lista= new ArrayList<Horarios>();
						try {
							date = (Date) formatoData.parse(data);
							
							h.setData(date);
							
							col= dao.getById(Integer.parseInt(idColaborador));
							dao.addAtendimento(col, h, c);
							lista=null;
							lista= dao.getHorarios(col);
							
							col.setHorarios(lista);
							System.out.println(h.getData());
							request.setAttribute("colaborador", col);
							request.setAttribute("mensagem", "Atendimento agendado com sucesso");
							request.getRequestDispatcher("condomino/index.jsp").forward(request, response);
							
							
							
							
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
						
					
					
				}
	}

}
