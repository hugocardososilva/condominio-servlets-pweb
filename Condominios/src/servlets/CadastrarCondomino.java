package servlets;

import java.io.IOException;

import model.Morador;
import model.Veiculo;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequestEvent;
import javax.servlet.ServletRequestListener;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.CondominoDAO;
import builder.CondominoBuilder;
import model.Condomino;
import email.EnviarEmail;


/**
 * Servlet implementation class CadastrarCondomino
 */
@WebServlet("/CadastrarCondomino.do")
public class CadastrarCondomino extends HttpServlet  {
	private static final long serialVersionUID = 1L;
	private EnviarEmail enviarEmail;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CadastrarCondomino() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ref = request.getParameter("ref");
		HttpSession session= request.getSession();
		if(ref.equalsIgnoreCase("inquilino")){
			request.getRequestDispatcher("condomino/adicionar-inquilino.jsp").forward(request, response);
		}else
			
				if(ref.equalsIgnoreCase("removerinquilino")){
					Condomino c = (Condomino) session.getAttribute("condomino");
					CondominoDAO dao = new CondominoDAO();
					c.setInquilino(null);
					try {
						dao.update(c);
						for(Morador m : c.getMoradores()){
							dao.removeMorador(m);
							
						}
						for(Veiculo v : c.getVeiculos()){
							dao.removeVeiculo(v);
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					request.setAttribute("mensagem", "Inquilino removido com sucesso");
					request.getRequestDispatcher("condomino/index.jsp").forward(request, response);
				}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		HttpSession session= request.getSession();
		
		String ref= request.getParameter("ref");
		
		
		if(ref.equalsIgnoreCase("update")){
			String proprietario= request.getParameter("proprietario");
			System.out.println(proprietario);
			String email= request.getParameter("email");
			int telefone = Integer.parseInt(request.getParameter("telefone"));
			String inquilino= request.getParameter("inquilino");
			String senha = request.getParameter("senha");
			String cpf= request.getParameter("cpf");
			CondominoDAO dao = new CondominoDAO();
			Condomino c = (Condomino) session.getAttribute("condomino");
			//System.out.println(c.toString());
			try {
				
					
			if (!senha.equalsIgnoreCase(c.getSenha())){
				c.setNovo(true);
			}
				
			
			
			c.setProprietario(proprietario);
			c.setEmail(email);
			c.setSenha(senha);
			c.setTelefone(telefone);
			c.setInquilino(inquilino);
			c.setCpf(Long.parseLong(cpf));
			System.out.println(c.toString());
			dao.update(c);
			
			session.setAttribute("condomino", c);
			//response.sendRedirect("/Condominios/condomino/index.jsp");
			request.setAttribute("mensagem", "Cond�mino atualizado");
			
			} catch (Exception e) {
				// TODO Auto-generated catch block
				System.out.println(e.toString());
			}
			request.getRequestDispatcher("/condomino/index.jsp").forward(request, response);
		
		
		}else 
			if (ref.equalsIgnoreCase("add")){
		CondominoDAO dao = new CondominoDAO();
		String cpf= request.getParameter("cpf");
		String ap= request.getParameter("numeroAp");
		String para= request.getParameter("email");
		String login= "apto"+ap;
		List<Condomino> lista= new ArrayList<Condomino>();
		
		try {
			lista= dao.getAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for(Condomino c: lista){
			if(Long.parseLong(cpf)== c.getCpf()){
				request.setAttribute("mensagem", "esse CPf ja existe");
				request.getRequestDispatcher("Contato.jsp").forward(request, response);
			}
		}
		
		
		Random rd= new Random();
		String gerarsenha= String.valueOf(rd.nextInt(999999));

		//
		CondominoBuilder cb= new CondominoBuilder(login)
		.email(request.getParameter("email")).alugado(false).apartamento(Integer.parseInt(ap))
		.revisado(false).senha(gerarsenha).proprietario(request.getParameter("proprietario")).cpf(Long.parseLong(cpf))
		.telefone(Integer.parseInt(request.getParameter("telefone")));
		
		Condomino c= new Condomino();
		c=cb.create();
		
		
		System.out.println(c.toString());
				
			
		
		
		dao.add(c);
		
		
		
		
		request.setAttribute("mensagem", "\n Aguarde a aprova��o de Cadastro pelo Administrador para acessar o site. ");
		RequestDispatcher dispacher= request.getRequestDispatcher("index.jsp");
		dispacher.forward(request, response);
		}	else
			
		
		if(ref.equalsIgnoreCase("addinquilino")){
			Condomino c = (Condomino) session.getAttribute("condomino");
			CondominoDAO dao = new CondominoDAO();
			String inquilino= request.getParameter("inquilino");
			c.setInquilino(inquilino);
			try {
				dao.update(c);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			request.setAttribute("mensagem", "Inquilino Cadastrado com sucesso");
			request.getRequestDispatcher("condomino/index.jsp").forward(request, response);
			
		}
	}



}
