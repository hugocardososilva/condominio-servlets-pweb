package servlets;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.view.JasperViewer;
import model.Condomino;
import model.GerarBoleto;
import model.Taxa;
import DAO.CondominoDAO;
import DAO.DAO;
import DAO.GerarRelatorio;
import DAO.TaxaDAO;

/**
 * Servlet implementation class CadastrarTaxa
 */
@WebServlet("/CadastrarTaxa.do")
public class CadastrarTaxa extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CadastrarTaxa() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NumberFormatException {
		String ref = request.getParameter("ref");
			if (ref.equalsIgnoreCase("visualisar")){
				TaxaDAO tdao= new TaxaDAO();
				List<Taxa> taxas= new ArrayList<Taxa>();
				
				try {
					taxas=tdao.getAll();
					

				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				request.setAttribute("taxas", taxas);
				request.getRequestDispatcher("admin/taxa-condominio.jsp").forward(request, response);
			}
			else
				if(ref.equalsIgnoreCase("condomino")){
					TaxaDAO dao = new TaxaDAO();
					List<Taxa> lista= new ArrayList<Taxa>();
					 try {
						 lista=dao.getAll();
						 
						
					} catch (Exception e) {
						// TODO: handle exception
					}
					 request.setAttribute("taxas", lista);
					 request.getRequestDispatcher("condomino/historico-taxa.jsp").forward(request, response);
					 
				}else 
					if ( ref.equalsIgnoreCase("receber")){
						System.out.println("recebendo pagamento");
						String id = request.getParameter("id");
						TaxaDAO dao = new TaxaDAO();
						
						
						try {
							for(Taxa t: dao.getAll()){
								if( Integer.parseInt(id)== t.getId()){
									request.setAttribute("taxa", t);
									System.out.println("peggou essa taxa" + t);
									
								}
							}
							
							
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						request.getRequestDispatcher("admin/receberpagamento.jsp").forward(request, response);
					}else
						if(ref.equalsIgnoreCase("boleto")){
							String id= request.getParameter("id");
							Taxa t = new Taxa();
							TaxaDAO dao= new TaxaDAO();
							try {
								t= dao.getById(Integer.parseInt(id));
								
							} catch (Exception e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
							GerarBoleto boleto= new GerarBoleto(t);
							boleto.gerarBoleto();
							
							List<Taxa> lista= new ArrayList<Taxa>();
							 try {
								 lista=dao.getAll();
								 
								
							} catch (Exception e) {
								// TODO: handle exception
							}
							 request.setAttribute("taxas", lista);
							request.getRequestDispatcher("condomino/historico-taxa.jsp").forward(request, response);
							
							
						}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ref = request.getParameter("ref");
		if (ref.equalsIgnoreCase("gerar")){
			TaxaDAO dao = new TaxaDAO();
			
			String dia = request.getParameter("dia");
			String mes= request.getParameter("mes");
			String ano= request.getParameter("ano");
			Float valor= Float.valueOf(request.getParameter("valor"));
			Float multa= Float.valueOf(request.getParameter("multa"));
			Taxa taxa = new Taxa();
			taxa.setTaxa(valor);
			taxa.setMulta(multa);
			
			
			SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
			String data = dia+"/"+mes+"/"+ano;
			
			CondominoDAO cdao= new CondominoDAO();
			
			
			List<Condomino> condominos= new ArrayList<Condomino>();
			Date date;
			
			try {
				date = (Date) formatoData.parse(data);
				for(Taxa t: dao.getAll()){
					Calendar taxac= Calendar.getInstance();
					taxac.setTime(t.getReferencia());
					Calendar data_param= Calendar.getInstance();
					data_param.setTime(date);
					if(data_param.get(Calendar.MONTH)== taxac.get(Calendar.MONTH)){
						request.setAttribute("taxas", dao.getAll());
						request.setAttribute("mensagem", "Esse mes ja foi gerado");
						request.getRequestDispatcher("admin/taxa-condominio.jsp").forward(request, response);
						return;
					}
					
				}
				taxa.setReferencia(date);
				taxa.setData_pagamento(null);
				condominos= cdao.getAll();
				dao.gerarTaxas((ArrayList<Condomino>) condominos, taxa);
				System.out.println(taxa.toString());
				
				request.setAttribute("taxas", dao.getAll());
				request.setAttribute("mensagem", "Taxa gerada com sucesso");
				request.getRequestDispatcher("admin/taxa-condominio.jsp").forward(request, response);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}else
			if(ref.equalsIgnoreCase("recebido")){
				String id = request.getParameter("id");
				String dia= request.getParameter("dia");
				String mes = request.getParameter("mes");
				String ano = request.getParameter("ano");
				SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
				String data = dia+"/"+mes+"/"+ano;
				TaxaDAO dao= new TaxaDAO();
				
				try {
					Date dataPagamento= (Date) formatoData.parse(data);
					for(Taxa t: dao.getAll()){
						if(t.getId()== Integer.parseInt(id)){
							t.setData_pagamento(dataPagamento);
							t.setPago(true);
							dao.update(t);
							request.setAttribute("taxas", dao.getAll());
							request.setAttribute("mensagem", "pagamento registrado com sucesso.");
							request.getRequestDispatcher("admin/taxa-condominio.jsp").forward(request, response);
						}
					}
					
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else
				if(ref.equalsIgnoreCase("visualizar_mes")){
					List<Taxa> taxas= new ArrayList<Taxa>();
					TaxaDAO dao = new TaxaDAO();
					String mes = request.getParameter("mes");
					List<Taxa> taxa_mes= new ArrayList<Taxa>();
					try {
						taxas= dao.getAll();
						SimpleDateFormat formatoData = new SimpleDateFormat("dd/MM/yyyy");
						Date data= (Date)formatoData.parse(mes);
						Calendar calendar= Calendar.getInstance();
						calendar.setTime(data);
						
							for(Taxa t: taxas){
								Calendar referencia= Calendar.getInstance();
								referencia.setTime(t.getReferencia());
								if(referencia.get(Calendar.MONTH)== calendar.get(Calendar.MONTH) && referencia.get(Calendar.YEAR)== calendar.get(Calendar.YEAR)){
									taxa_mes.add(t);
								}
							}
				
					
					
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					request.setAttribute("taxas", taxa_mes);
					request.getRequestDispatcher("admin/taxa-condominio.jsp").forward(request, response);
				}else
					if(ref.equalsIgnoreCase("pdf")){
						GerarRelatorio gerar= new GerarRelatorio();
						try {
							gerar.gerar();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
					}
	}

}
