package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;



import model.Colaborador;
import model.Condomino;
import model.Horarios;
import model.Mensagem;

import DAO.ColaboradorDAO;
import DAO.CondominoDAO;
import DAO.MensagensDAO;

/**
 * Servlet implementation class LogarCondomino
 */
@WebServlet("/LogarCondomino.do")
public class LogarCondomino extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LogarCondomino() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, NullPointerException {
		response.setContentType("text/html");
		
		String login= request.getParameter("login");
		String senha = request.getParameter("senha");
		System.out.println(login+" dentro do login " + senha);
		CondominoDAO dao = new CondominoDAO();
		HttpSession session= request.getSession();
		MensagensDAO mdao= new MensagensDAO();
		List<Mensagem> mensagens= new ArrayList<Mensagem>();
		ColaboradorDAO coldao= new ColaboradorDAO();
			
		
		Condomino c = new Condomino();
		
			try{
				c= dao.login(login, senha); 
				//System.out.println(c.toString());
				if(c != null){
					c.setVeiculos(dao.getVeiculos(c));
					c.setMoradores(dao.getMoradores(c));
					mensagens= mdao.getMinhasMensagens(c.getId());
					List<Mensagem> minhalista= new ArrayList<Mensagem>();
					for(Mensagem m : mensagens){
						if(m.isLido()== false)
							minhalista.add(m);
					}
					if(c.isNovo()== false){
						request.setAttribute("mensagem", "Altere sua senha, clique no menu alterar cadastro." +
								"Enquanto n�o for alterada, essa mensagem aparecer� a cada login");
					}
					List<Horarios> horarios = new ArrayList<Horarios>();
					for(Colaborador col : coldao.getAll()){
						for(Horarios  h :coldao.getHorarios(col)){
							if(h.getCondomino()!= null)	{
								if(h.getCondomino().getId()== c.getId()){
									
									horarios.add(h);
									}
								}
						}
						
					
					}
					session.setAttribute("condomino", c);
					session.setAttribute("listaMensagem", minhalista.size());
					session.setAttribute("horarios", horarios);
					System.out.println(mensagens.toString());
					request.getRequestDispatcher("condomino/index.jsp").forward(request, response);
				
				
			}else{
					request.setAttribute("mensagem", "Login ou senha incorretos");
					request.getRequestDispatcher("index.jsp").forward(request, response);
			
		
	}
			}catch(NullPointerException  e){
				e.printStackTrace();
				
				
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	}
}
