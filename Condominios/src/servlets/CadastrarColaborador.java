package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import model.Colaborador;

import DAO.ColaboradorDAO;
import builder.ColaboradorBuilder;

/**
 * Servlet implementation class CadastrarColaborador
 */
@WebServlet("/CadastrarColaborador.do")
public class CadastrarColaborador extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public CadastrarColaborador() {
        super();
    }
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		String nome= request.getParameter("nome");
		String cpf= request.getParameter("cpf");
		String telefone= request.getParameter("telefone");
		String servico= request.getParameter("servico");
		System.out.println(nome+ cpf+ telefone+ servico);
		
		Colaborador c = new Colaborador();
		c.setNome(nome);
		c.setCpf(Long.parseLong(cpf));
		c.setTelefone(Integer.parseInt(telefone));
		c.setServico(servico);
		
		
		
		ColaboradorDAO dao= new ColaboradorDAO();
		List<Colaborador> lista = new ArrayList<Colaborador>();
		try {
			dao.add(c); 
			
			 lista= dao.getAll();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.out.println("dentro do cadastro "+ c.toString());
		request.setAttribute("colaboradores", lista);
		request.setAttribute("mensagem", "Colaborador cadastrador com sucesso");
		request.getRequestDispatcher("admin/listar-colaboradores.jsp").forward(request, response);	
		
		
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		String ref = req.getParameter("ref");
			if(ref.equalsIgnoreCase("add")){
				req.getRequestDispatcher("admin/CadastrarColaborador.jsp").forward(req, resp);
			}
	}

}
