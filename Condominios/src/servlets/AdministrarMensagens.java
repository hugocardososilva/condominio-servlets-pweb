package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import DAO.CondominoDAO;
import DAO.MensagensDAO;

import model.Condomino;
import model.Mensagem;

/**
 * Servlet implementation class AdministrarMensagens
 */
@WebServlet("/AdministrarMensagens.do")
public class AdministrarMensagens extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public AdministrarMensagens() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ref= request.getParameter("ref");
		HttpSession session= request.getSession();
		if(ref.equalsIgnoreCase("mensagens")){
			
			try {
			Condomino c= new Condomino();
			c= (Condomino) session.getAttribute("condomino");
			MensagensDAO mdao= new MensagensDAO();
			List<Mensagem> mensagens= new ArrayList<Mensagem>();
			mensagens= mdao.getMinhasMensagens(c.getId());
			
				
				session.setAttribute("mensagens", mensagens);
				System.out.println("minhas mensagens " + mensagens);
				request.getRequestDispatcher("condomino/visualizar-mensagens.jsp").forward(request, response);
			} catch (Exception e) {
				// TODO: handle exception
			}
		}else
			if(ref.equalsIgnoreCase("ler")){
				int id_mensagem= Integer.parseInt(request.getParameter("id_mensagem"));
				int id_condomino= Integer.parseInt(request.getParameter("id_cond"));
				try {
					
					MensagensDAO dao = new MensagensDAO();
					List<Mensagem> mensagens= new ArrayList<Mensagem>();
					dao.lerMensagem(id_mensagem, id_condomino);
					mensagens= dao.getMinhasMensagens(id_condomino);
					
				
					
					System.out.println(mensagens.toString());
					session.setAttribute("mensagens", mensagens);
					request.getRequestDispatcher("condomino/visualizar-mensagens.jsp").forward(request, response);
					
				} catch (Exception e) {
					// TODO: handle exception
				}
				
			}else
				if(ref.equalsIgnoreCase("administrar")){
					MensagensDAO dao = new MensagensDAO();
					List<Mensagem> mensagens= new ArrayList<Mensagem>();
					try {
						mensagens=dao.getAll();
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					request.setAttribute("mensagens", mensagens);
					request.getRequestDispatcher("admin/enviar-mensagem.jsp").forward(request, response);
					
					
				}else
					if(ref.equalsIgnoreCase("cond_excluir")){
						String mensagem= request.getParameter("id_mensagem");
						String condomino = request.getParameter("id_cond");
						MensagensDAO dao = new MensagensDAO();
						List<Mensagem> mensagens= new ArrayList<Mensagem>();
					
						
						try {
							dao.excluirMensagem(Integer.parseInt(mensagem), Integer.parseInt(condomino));
							mensagens= dao.getMinhasMensagens(Integer.parseInt(condomino));
						} catch (NumberFormatException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						session.setAttribute("mensagens", mensagens);
						request.setAttribute("mensagem", "mensagem excluida com sucesso.");
						request.getRequestDispatcher("condomino/visualizar-mensagens.jsp").forward(request, response);
				}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String ref = request.getParameter("ref");
			if(ref.equalsIgnoreCase("add")){
				String titulo= request.getParameter("titulo");
				String assunto= request.getParameter("assunto");
				String mensagem= request.getParameter("mensagem");
				Mensagem m = new Mensagem();
				m.setAssunto(assunto);
				m.setTitulo(titulo);
				m.setMensagem(mensagem);
				
				String[] destinatarios= request.getParameterValues("condomino");
				System.out.println(destinatarios[0]);
				MensagensDAO dao = new MensagensDAO();
				CondominoDAO daocon= new CondominoDAO();
				ArrayList<Condomino> lista= new ArrayList<Condomino>();
				
				try {
					for(int i =  0; i< destinatarios.length; i++){
						Condomino c = new Condomino();
					
						c= daocon.getById(Integer.parseInt(destinatarios[i]));
						lista.add(c);
						
					}
					m.setDestinatarios(lista);
					dao.add(m);
					request.setAttribute("mensagem", "Mensagem enviada com sucesso");
					request.getRequestDispatcher("admin/enviar-mensagem.jsp").forward(request, response);
						
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
				
				
			}
				
	}

}
