package servlets;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Condomino;
import model.Morador;
import model.Veiculo;
import DAO.CondominoDAO;

/**
 * Servlet implementation class Moradores
 */
@WebServlet("/Moradores.do")
public class Moradores extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Moradores() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String ref= request.getParameter("ref");
		HttpSession session = (HttpSession) request.getSession();
		if(ref.equalsIgnoreCase("morador")){
			request.getRequestDispatcher("condomino/adicionar-morador.jsp").forward(request, response);
		}else
			if(ref.equalsIgnoreCase("removermorador")){
				String id= request.getParameter("morador");
				Morador morador= new Morador();
				morador.setId(Integer.parseInt(id));
				CondominoDAO dao = new CondominoDAO();
				Condomino c = new Condomino();
			 c =(Condomino) session.getAttribute("condomino");
				List<Morador> moradores= new ArrayList<Morador>();
				try {
					dao.removeMorador(morador);
					moradores= dao.getMoradores(c);
					c.setMoradores(null);
					c.setMoradores(moradores);
					session.setAttribute("condomino", c);
					
					request.setAttribute("mensagem", "morador removido");
					request.getRequestDispatcher("condomino/index.jsp").forward(request, response);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		HttpSession session = request.getSession();
		String ref = request.getParameter("ref");
		
		
		
		if(ref.equalsIgnoreCase("add")){
			
			Condomino c = (Condomino) session.getAttribute("condomino");
			String morador= request.getParameter("nome");
			CondominoDAO dao = new CondominoDAO();
			
			
				try {
					dao.addMorador(c, morador);
					c.setMoradores(null);
					c.setMoradores(dao.getMoradores(c));
					session.setAttribute("condomino", c);
					
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					System.out.println(e.toString());
				}
			
			//request.getRequestDispatcher("condomino/index.jsp").forward(request, response);
				response.sendRedirect("condomino/index.jsp");
		}
	}
	}


