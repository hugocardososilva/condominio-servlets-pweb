package builder;

import java.util.Date;
import java.util.List;

import model.Colaborador;
import model.Horarios;

public class ColaboradorBuilder implements BuilderColaborador<ColaboradorBuilder, Colaborador>{
	private int id, telefone, cpf;
	private String nome, servico;
	private List<Horarios> horarios;
	
	public ColaboradorBuilder(int cpf){
		this.cpf = cpf;
	}
	@Override
	public ColaboradorBuilder id(int id) {
		this.id= id;
		return this;
	}

	@Override
	public ColaboradorBuilder telefone(int telefone) {
		this.telefone= telefone;
		return this;
	}

//	@Override
//	public ColaboradorBuilder cpf(int cpf) {
//		this.cpf
//	}

	@Override
	public ColaboradorBuilder nome(String nome) {
		this.nome= nome;
		return this;
	}

	@Override
	public ColaboradorBuilder servico(String servico) {
		this.servico= servico;
		return this;
	}

	@Override
	public ColaboradorBuilder horarios(List<Horarios> horarios) {
		this.horarios= horarios;
		return this;
	}

	@Override
	public Colaborador create() {
		Colaborador c= new Colaborador();
		c.setId(this.id);
		c.setNome(this.nome);
		c.setHorarios(this.horarios);
		c.setServico(this.servico);
		c.setTelefone(this.telefone);
		return c;
	}

}
