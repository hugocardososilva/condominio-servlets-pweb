package builder;

import java.util.Date;
import java.util.List;

import model.Horarios;

public interface BuilderColaborador <K,T>{
	public K id(int id);
	public K telefone(int telefone);
	//public K cpf(int cpf);
	public K nome(String nome);
	public K servico(String servico);
	public K horarios(List<Horarios> horarios);
	public T create();

}
