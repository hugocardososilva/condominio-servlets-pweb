package builder;

import java.util.List;

import model.Condomino;
import model.Morador;
import model.Veiculo;

public class CondominoBuilder implements BuilderCondomino<CondominoBuilder, Condomino> {
	private int id, apartamento;
	private long telefone, cpf;
	private String proprietario,email,inquilino, senha, login;
	private boolean alugado, revisado,novo;
	private List<Morador> moradores;
	private List<Veiculo> veiculos;
	
	public CondominoBuilder(String login){
		this.login= login;
	}
	@Override
	public CondominoBuilder id(int id) {
		this.id= id;
		return this;
	}
	@Override
	public CondominoBuilder telefone(long telefone) {
		this.telefone= telefone;
		return this;
	}
	@Override
	public CondominoBuilder apartamento(int apartamento) {
		this.apartamento= apartamento;
		return this;
		
	}
	@Override
	public CondominoBuilder proprietario(String proprietario) {
		this.proprietario= proprietario;
		return this;
		
	}
	@Override
	public CondominoBuilder email(String email) {
		this.email= email;
		return this;
	}
	@Override
	public CondominoBuilder inquilino(String inquilino) {
		this.inquilino= inquilino;
		return this;
	}
	@Override
	public CondominoBuilder senha(String senha) {
		this.senha= senha;
		return this;
	}
	@Override
	public CondominoBuilder moradores(List<Morador> moradores) {
		this.moradores= moradores;
		return this;
	}
	@Override
	public CondominoBuilder veiculo(List<Veiculo> veiculos) {
		this.veiculos= veiculos;
		return this;
	}
	@Override
	public CondominoBuilder alugado(boolean alugado) {
		this.alugado= alugado;
		return this;
	}
	@Override
	public CondominoBuilder revisado(boolean revisado) {
		this.revisado= revisado;
		return this;
	}
	@Override
	public CondominoBuilder novo(boolean novo) {
		this.novo= novo;
		return this;
	}
	@Override
	public Condomino create() {
		Condomino c= new Condomino();
		c.setId(this.id);
		c.setAlugado(this.alugado);
		c.setApartamento(this.apartamento);
		c.setEmail(this.email);
		c.setInquilino(this.inquilino);
		c.setLogin(this.login);
		c.setMoradores(this.moradores);
		c.setProprietario(this.proprietario);
		c.setRevisado(this.revisado);
		c.setSenha(this.senha);
		c.setTelefone(this.telefone);
		c.setVeiculos(this.veiculos);
		c.setNovo(this.novo);
		c.setCpf(this.cpf);
		return c;
	}
	@Override
	public CondominoBuilder cpf(long cpf) {
		this.cpf= cpf;
		return this;
	}
	
}
