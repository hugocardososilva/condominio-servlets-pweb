package builder;

import java.util.List;

import model.Morador;
import model.Veiculo;

public interface BuilderCondomino<K,T> {
	public K id(int id);
	public K telefone(long telefone);
	public K apartamento(int apartamento);
	public K proprietario(String proprietario);
	public K email(String email);
	public K inquilino(String inquilino);
	public K senha(String senha);
	public K moradores(List<Morador> moradores);
	public K veiculo(List<Veiculo> veiculos);
	public K alugado(boolean alugado);
	public K revisado(boolean revisado);
	public K novo (boolean novo);
	public K cpf(long cpf);
	public T create();
	
	

}
