package crud;

import java.util.List;

public interface Crud {
	public void add(Object crud) throws Exception;
	public void remove(Object crud) throws Exception;
	public void update(Object crud) throws Exception;
	public Object getById(int id) throws Exception;
	public List<?> getAll() throws Exception;

}
