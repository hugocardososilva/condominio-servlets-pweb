package filtros;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Enumeration;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Condomino;

import DAO.CondominoDAO;

/**
 * Servlet Filter implementation class ValidarCadastro
 */

public class ValidarCadastro implements Filter {

    /**
     * Default constructor. 
     */
    public ValidarCadastro() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		Enumeration<String> e = request.getParameterNames();
		while(e.hasMoreElements()){
			Object nomes = e.nextElement();
			String nome= (String) nomes;
			System.out.println(nome);
		
		}
		
		HttpServletRequest req= (HttpServletRequest) request;
		HttpServletResponse res= (HttpServletResponse) response;
		System.out.println("testando filtros de cadastro");
		String apto = request.getParameter("numeroAp");
		
		if(apto!=null){
			CondominoDAO dao = new CondominoDAO();
			try {
				ArrayList<Condomino> lista = (ArrayList<Condomino>) dao.getAll();
				
				System.out.println(apto + "esse apto");
			for (Condomino c: lista){
				if (c.getApartamento()== Integer.parseInt(apto)){
					
					HttpSession session = req.getSession();
					session.setAttribute("mensagem", "Esse apartamento j� existe, por favor, entre em contato com o administrador");
					session.setAttribute("proprietario", req.getParameter("proprietario"));
					session.setAttribute("email", req.getParameter("email"));
					session.setAttribute("telefone", req.getParameter("telefone	"));
					res.sendRedirect("Contato.jsp");
					return;
				}
				
			}
			
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
		}
		chain.doFilter(request, response);
		}
		
	

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
