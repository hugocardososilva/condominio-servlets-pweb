package filtros;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class ValidarEditCondomino
 */

public class ValidarEditCondomino implements Filter {

    /**
     * Default constructor. 
     */
    public ValidarEditCondomino() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		String ref=request.getParameter("ref");
		System.out.println("no filtro de editar cadastro");
		HttpServletResponse res= (HttpServletResponse) response;
		if(ref.equalsIgnoreCase("update")){
			String senha= request.getParameter("senha");
			String repete_senha= request.getParameter("repete_senha");
				if(!senha.equalsIgnoreCase(repete_senha)){
					request.setAttribute("mensagem", "as senhas sao diferentes");
					res.sendRedirect("condomino/editar-condomino.jsp");
				}
		}
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
