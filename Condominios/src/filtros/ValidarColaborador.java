package filtros;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import model.Colaborador;
import model.Condomino;
import DAO.ColaboradorDAO;
import DAO.CondominoDAO;

/**
 * Servlet Filter implementation class ValidarColaborador
 */

public class ValidarColaborador implements Filter {

    /**
     * Default constructor. 
     */
    public ValidarColaborador() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req= (HttpServletRequest) request;
		HttpServletResponse res= (HttpServletResponse) response;
		System.out.println("testando filtros de cadastro");
		String cpf = request.getParameter("cpf");
		
		if(cpf!=null){
			ColaboradorDAO dao = new ColaboradorDAO();
			try {
				ArrayList<Colaborador> lista = (ArrayList<Colaborador>) dao.getAll();
				
				System.out.println(cpf + "esse cpf");
			for (Colaborador c: lista){
				if (c.getCpf()== Integer.parseInt(cpf)){
					
					
					req.setAttribute("mensagem", "Esse cpf j� existe.");
					
					
					return;
				}
				
			}
			
			} catch (Exception ex) {
				// TODO Auto-generated catch block
				ex.printStackTrace();
			}
		}
		chain.doFilter(req, res);
		}
		
	

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
