package filtros;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import email.EnviarEmail;
import model.Condomino;
import DAO.CondominoDAO;

/**
 * Servlet Filter implementation class ReenviarEmail
 */

public class ReenviarEmail implements Filter {

    /**
     * Default constructor. 
     */
    public ReenviarEmail() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
		
		
		
		HttpServletRequest request=(HttpServletRequest) req;
		HttpServletResponse response= (HttpServletResponse) res;
		
		String email= request.getParameter("reenviar");
		CondominoDAO dao = new CondominoDAO();
		List<Condomino> lista= new ArrayList<Condomino>();
		try {
			lista= dao.getAll();
			for (Condomino c : lista){
				if(c.getEmail().equalsIgnoreCase(email)){
					StringBuffer sb= new StringBuffer();
					sb.append("Ola, bem vindo ao sistema de condominio\n");
					sb.append("Seu login:\n");
					sb.append(c.getLogin()+" \n");
					sb.append("sua senha: ");
					sb.append(c.getSenha() + "\n");
					sb.append("Proprietario: " + c.getProprietario());
					sb.append("\nEmail: " + c.getEmail());
					sb.append("\n Telefone: " + c.getTelefone());
					sb.append("\n n Clique no link para ir a p�gina http://localhost:8082/Condominio");
					String assunto = sb.toString();
					String titulo = "Reenviar senha";
					ServletContext sc = req.getServletContext();
					EnviarEmail enviarEmail= new EnviarEmail();
					enviarEmail.Enviar(c.getEmail(), assunto, titulo, sc);
					request.setAttribute("mensagem", "Foi enviado um email com as defini��es de login");
					request.getRequestDispatcher("enviar.jsp").forward(request, response);
					
					
				}else{
					request.setAttribute("mensagem", "Erro: email n�o encontrado no banco de dados");
					request.getRequestDispatcher("reenviar-senha.jsp").forward(request, response);
					
				}
					
			}
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		chain.doFilter(req, res);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
		// TODO Auto-generated method stub
	}

}
