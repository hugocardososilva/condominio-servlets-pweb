package DAO;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import model.Taxa;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRResultSetDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

import com.mysql.jdbc.PreparedStatement;
import com.mysql.jdbc.Statement;

public class GerarRelatorio extends DAO {
	
	public void gerar() throws Exception{
	TaxaDAO dao = new TaxaDAO();
	List<Taxa> lista = dao.getAll();
	
		JasperReport pathjrxml = JasperCompileManager.compileReport("reports/Repporte.jrxml");
		JasperPrint printReport = JasperFillManager.fillReport(pathjrxml, null, new JRBeanCollectionDataSource(lista));
		JasperExportManager.exportReportToPdfFile(printReport, "relatorio/reportex.pdf");
		System.out.println("Relatorio gerado");
		JasperReport report = JasperCompileManager
				.compileReport("reports/Repporte.jrxml");
		JasperPrint print = JasperFillManager.fillReport(report, null,
				new JRBeanCollectionDataSource(lista));	
		JasperExportManager.exportReportToPdfFile(print,
				"reports/RelatorioUser.pdf");
}
}