package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import model.Administrador;
import model.Condomino;
import builder.CondominoBuilder;



import crud.Crud;

public class AdministradorDAO extends DAO implements Crud {

	@Override
	public void add(Object crud) {
	
		
	}

	@Override
	public void remove(Object crud) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Object crud) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object getById(int id) {
		return id;
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<?> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

	public Administrador login(final String email, final String senha) throws Exception{
		PreparedStatement stmt= null;
		try {
			System.out.println(email+"  " + senha);
			open();
			stmt= con.prepareStatement("SELECT * FROM admin WHERE email like ? and senha like ?");
			stmt.setString(1, email );
			stmt.setString(2, senha);
			ResultSet rs= stmt.executeQuery();
			if(rs.next()){
				Administrador a = new Administrador();
				a.setId(rs.getInt("id"));
				a.setEmail(rs.getString("email"));
				a.setNome(rs.getString("nome"));
				a.setSenha(rs.getString("senha"));
				con.close();
				return a;
			}
			con.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
			throw new RuntimeException(e);
		}
		
		con.close();
		return null;
	}
	

}
