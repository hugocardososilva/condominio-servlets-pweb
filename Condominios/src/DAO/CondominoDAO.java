package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


import model.Condomino;
import model.Morador;
import model.Veiculo;

import builder.CondominoBuilder;

import crud.Crud;


public class CondominoDAO extends DAO implements Crud {

	@Override
	public void add(Object crud) {
		PreparedStatement stmt;
		try {
			Condomino c = (Condomino) crud;
			open();
			stmt= con.prepareStatement("INSERT INTO condomino" +
					"(login, senha, telefone," +
					"apartamento, proprietario, email," +
					"inquilino, alugado, revisado, cpf) VALUES" +
					"(?,?,?,?,?,?,?,?,?,?)");
			stmt.setString(1, c.getLogin());
			stmt.setString(2, c.getSenha());
			stmt.setLong(3, c.getTelefone());
			stmt.setInt(4, c.getApartamento());
			stmt.setString(5, c.getProprietario());
			stmt.setString(6, c.getEmail());
			stmt.setString(7, c.getInquilino());
			stmt.setBoolean(8, c.isAlugado());
			stmt.setBoolean(9, c.isRevisado());
			stmt.setLong(10,c.getCpf());
			stmt.execute();
			
			//healm
			stmt.close();
			stmt=null;
			stmt= con.prepareStatement("INSERT INTO usuarios(email, senha)" +
					"VALUES (?,?)");
			stmt.setString(1, c.getLogin());
			stmt.setString(2, c.getSenha());
			stmt.execute();
			stmt.close();
			stmt= null;
			stmt= con.prepareStatement("INSERT INTO privilegios(email, papel)" +
					"VALUES(?,?)");
			stmt.setString(1, c.getLogin());
			stmt.setString(2, "condomino");
			stmt.execute();
			stmt.close();
			con.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		}
		 catch (Exception e) {
				System.out.println(e.toString());
			}
		
	}

	@Override
	public void remove(Object crud) {
		Condomino c = (Condomino) crud;
		PreparedStatement stmt= null;
		try {
			open();
			stmt=con.prepareStatement("DELETE FROM condomino" +
					"WHERE id =  ?");
			stmt.setInt(1, c.getId());
			stmt.execute();
			stmt.close();
			con.close();
			
		} catch (SQLException e) {
			System.out.println(e.toString());
		}
		 catch (Exception e) {
				System.out.println(e.toString());
			}
		
	}

	@Override
	public void update( final Object crud) throws Exception {
		Condomino c = (Condomino) crud;
		PreparedStatement stmt= null;
		
			open();
			try {
			 stmt= (PreparedStatement) con.prepareStatement("UPDATE condomino set login = ?, senha =? , telefone =? , apartamento = ? , proprietario = ? , email = ? , inquilino = ? , alugado = ? , revisado = ? , novo = ? , cpf = ? WHERE id = ? ");
			stmt.setString(1, c.getLogin());
			stmt.setString(2, c.getSenha());
			stmt.setLong(3, c.getTelefone());
			stmt.setInt(4, c.getApartamento());
			stmt.setString(5, c.getProprietario());
			stmt.setString(6, c.getEmail());
			stmt.setString(7, c.getInquilino());
			stmt.setBoolean(8, c.isAlugado());
			stmt.setBoolean(9, c.isRevisado());
			stmt.setBoolean(10, c.isNovo());
			stmt.setLong(11, c.getCpf());
			stmt.setInt(12, c.getId());
			
			stmt.executeUpdate();
			stmt.close();
			stmt= null;
			 stmt= (PreparedStatement) con.prepareStatement("UPDATE usuarios set senha =?  WHERE email = ? ");
			
				stmt.setString(1, c.getSenha());
				stmt.setString(2, c.getLogin());
				stmt.executeUpdate();
				stmt.close();
				
				con.close();	
			
		} catch (SQLException e) {
			System.out.println(e.toString());
			throw new RuntimeException(e);
		}
		
		
		
	}

	@Override
	public Condomino getById(final int id) throws  Exception{
		PreparedStatement stmt= null;
		try {
			
			open();
			stmt= con.prepareStatement("SELECT * FROM condomino WHERE id = ? ");
			stmt.setInt(1, id );
			
			ResultSet rs= stmt.executeQuery();
			if(rs.next()){
				CondominoBuilder cb = new CondominoBuilder(rs.getString("login"));
				cb.id(rs.getInt("id"))
				.senha(rs.getString("senha"))
				.email(rs.getString("email"))
				.inquilino(rs.getString("inquilino"))
				.telefone(rs.getInt("telefone"))
				.apartamento(rs.getInt("apartamento"))
				.proprietario(rs.getString("proprietario"))
				.alugado(rs.getBoolean("alugado"))
				.revisado(rs.getBoolean("revisado"))
				.cpf(rs.getInt("cpf"));
				con.close();
				return cb.create();
			}
			con.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
			throw new RuntimeException(e);
		}
		
		con.close();
		return null;
		
	}

	@Override
	public List<Condomino> getAll() throws Exception {
		PreparedStatement stmt;
		
		try {
			List<Condomino> lista = new ArrayList<Condomino>();
			open();
			 stmt= con.prepareStatement("SELECT id, login, senha, telefone," +
					"apartamento, proprietario, email," +
					"inquilino, alugado, revisado, cpf FROM condomino");
			ResultSet rs= stmt.executeQuery();
			while(rs.next()){
				CondominoBuilder cb= new CondominoBuilder(rs.getString(2));
				cb.id(rs.getInt(1))
				.senha(rs.getString(3))
				.telefone(rs.getInt(4))
				.apartamento(rs.getInt(5))
				.proprietario(rs.getString(6))
				.email(rs.getString(7))
				.inquilino(rs.getString(8))
				.alugado(rs.getBoolean(9))
				.revisado(rs.getBoolean(10))
				.cpf(rs.getLong(11));
				lista.add(cb.create());
			} 
			stmt.close();
			con.close();
			return lista;
			
			
		} catch (SQLException e) {
			System.out.println(e.toString());
		
		 }
		con.close();
		return null;
	}
	
// efetuar login
	public Condomino login(final String login, final String senha) throws Exception{
		PreparedStatement stmt= null;
		open();
		try {
			System.out.println(login+"  " + senha);
			
			stmt= con.prepareStatement("SELECT * FROM condomino WHERE login like ? and senha like ?");
			stmt.setString(1, login );
			stmt.setString(2, senha);
			ResultSet rs= stmt.executeQuery();
			if(rs.next()){
				CondominoBuilder cb = new CondominoBuilder(login);
				cb.id(rs.getInt("id"))
				.senha(rs.getString("senha"))
				.email(rs.getString("email"))
				.inquilino(rs.getString("inquilino"))
				.telefone(rs.getInt("telefone"))
				.apartamento(rs.getInt("apartamento"))
				.proprietario(rs.getString("proprietario"))
				.alugado(rs.getBoolean("alugado"))
				.revisado(rs.getBoolean("revisado"))
				.novo(rs.getBoolean("novo"))
				.cpf(rs.getInt("cpf"));
				
				con.close();
				return cb.create();
				
			}
			con.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
			throw new RuntimeException(e);
		}
		
		con.close();
		return null;
	}
	
	public void addVeiculo(Condomino c, Veiculo v) throws Exception{
		PreparedStatement stmt = null;
		System.out.println(c.toString() + v.toString());
		try{	
			open();
			stmt= con.prepareStatement("INSERT INTO veiculo (cor, marca, placa, modelo, idCondomino)" +
					"VALUES(?,?,?,?,?)");
			
			stmt.setString(1, v.getCor());
			stmt.setString(2, v.getMarca());
			stmt.setString(3, v.getPlaca());
			stmt.setString(4, v.getModelo());
			stmt.setInt(5, c.getId());
			stmt.execute();
			stmt.close();
			
		
		
			con.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		throw new RuntimeException(e);
	}
	
	con.close();
	
}
	public List<Veiculo> getVeiculos(Condomino c) throws Exception{
		
		PreparedStatement stmt;
		open();
		try{
			
			List<Veiculo> lista = new ArrayList<Veiculo>();
			
			stmt= con.prepareStatement("SELECT id, cor, marca, placa, modelo, idCondomino" +
					" FROM veiculo WHERE idCondomino = "+c.getId());
			ResultSet rs= stmt.executeQuery();
			while(rs.next()){
				Veiculo v= new Veiculo();
				v.setId(rs.getInt(1));
				v.setCor(rs.getString(2));
				v.setMarca(rs.getString(3));
				v.setModelo(rs.getString(4));
				v.setPlaca(rs.getString(5));
				lista.add(v);
			} 
			stmt.close();
			con.close();
			return lista;
			
		
		
		
	} catch (SQLException e) {
		System.out.println(e.toString());
	//throw new RuntimeException(e);
	
}
return null;


}
	public void addMorador(Condomino c, String morador) throws Exception{
		PreparedStatement stmt = null;
		System.out.println(c.toString() + morador);
		try{	
			open();
			stmt= con.prepareStatement("INSERT INTO moradores (nome, id_condomino)" +
					"VALUES(?,?)");
			
			stmt.setString(1, morador);
			
			stmt.setInt(2, c.getId());
			stmt.execute();
			stmt.close();
			
		
		
			con.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		throw new RuntimeException(e);
	}
	
	con.close();
	
	}
	public void removeMorador(final Morador morador) throws Exception{
		
		PreparedStatement stmt= null;
		open();
		try {
			
			stmt= (PreparedStatement) con.prepareStatement("DELETE FROM moradores WHERE id=?");
			
			stmt.setInt(1, morador.getId());
			stmt.executeUpdate();
			stmt.close();
			con.close();
			
		} catch (SQLException e) {
			System.out.println(e.toString());
			//throw new RuntimeException();
		
			}
		
	}
public List<Morador> getMoradores(Condomino c) throws Exception{
		
		PreparedStatement stmt;
		open();
		try{
			
			List<Morador> lista = new ArrayList<Morador>();
			
			stmt= con.prepareStatement("SELECT id, nome, id_condomino" +
					" FROM moradores WHERE id_condomino = "+c.getId());
			ResultSet rs= stmt.executeQuery();
			while(rs.next()){
			Morador morador = new Morador();	
			//String morador= rs.getString(2);
			morador.setId(rs.getInt("id"));
			morador.setNome(rs.getString("nome"));
				lista.add(morador);
			} 
			stmt.close();
			con.close();
			return lista;
			
		
		
		
	} catch (SQLException e) {
		System.out.println(e.toString());
	//throw new RuntimeException(e);
	
}
return null;


}
public void removeVeiculo(final Veiculo veiculo) throws Exception{
	
	PreparedStatement stmt= null;
	open();
	try {
		
		stmt= (PreparedStatement) con.prepareStatement("DELETE FROM veiculo WHERE id=?");
		
		stmt.setInt(1, veiculo.getId());
		stmt.executeUpdate();
		stmt.close();
		con.close();
		
	} catch (SQLException e) {
		System.out.println(e.toString());
		//throw new RuntimeException();
	
		}
	
}
	
}	
	

 