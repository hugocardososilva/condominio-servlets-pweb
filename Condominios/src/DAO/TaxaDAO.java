package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import builder.CondominoBuilder;

import model.Colaborador;
import model.Condomino;
import model.Taxa;

import crud.Crud;

public class TaxaDAO extends DAO implements Crud {

	@Override
	public void add(Object crud) {
		PreparedStatement stmt;
		try {
			Taxa t = (Taxa) crud;
			System.out.println(t.toString());
			open();
			stmt= con.prepareStatement("INSERT INTO taxa" +
					"(referencia, valor, multa, id_condomino) VALUES" +
					"(?,?,?,?)");
			//java.sql.Date dt= new java.sql.Date(t.getReferencia().getTime());
			java.sql.Timestamp time= new java.sql.Timestamp(t.getReferencia().getTime());
			stmt.setTimestamp(1, time);
			List<Taxa> minhasTaxas= new ArrayList<Taxa>();
			for(Taxa taxa: this.getAll()){
				if(t.getCondomino()== taxa.getCondomino()){
					minhasTaxas.add(taxa);
				}
				
				
				
			}
			Taxa calcMulta = new Taxa();
			if(!minhasTaxas.isEmpty()){
			calcMulta= minhasTaxas.get(minhasTaxas.size()); //pegar ultima taxa
			System.out.println("calcmulta  " + calcMulta);
			
			
			Calendar data_pagamento= Calendar.getInstance();
			Calendar referencia= Calendar.getInstance();
			data_pagamento.setTime(calcMulta.getData_pagamento());
			referencia.setTime(calcMulta.getReferencia());
				if(data_pagamento.after(referencia) ){
					int dias= 0;
					for(Calendar i= (Calendar)referencia.clone(); !i.after(data_pagamento); i.add(Calendar.DAY_OF_MONTH, 1)){
						if(i.get(Calendar.DAY_OF_WEEK)!= Calendar.SATURDAY || i.get(Calendar.DAY_OF_WEEK)!= Calendar.SUNDAY){
							dias++;
						}
					}
					t.setTaxa(t.getTaxa()+(t.getMulta()*dias));
					stmt.setFloat(2, t.getTaxa());
				}
				}else{
			stmt.setFloat(2, t.getTaxa());
				}
			stmt.setFloat(3, t.getMulta());
			stmt.setInt(4, t.getCondomino().getId());
			
			stmt.execute();
			
			
			stmt.close();
			con.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		}
		 catch (Exception e) {
				System.out.println(e.toString());
			}
		

		
	}

	@Override
	public void remove(Object crud) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(Object crud) throws Exception {
		Taxa t = new Taxa();
		t= (Taxa) crud;
		PreparedStatement stmt= null;
		
			open();
			try {
			 stmt= (PreparedStatement) con.prepareStatement("UPDATE taxa set referencia = ?, valor =? , multa =? , pago = ? , data_pagamento = ? , multa_pagar = ? WHERE id = ? ");
			 java.sql.Timestamp referencia= new java.sql.Timestamp(t.getReferencia().getTime());
			 stmt.setTimestamp(1, referencia);
			 stmt.setFloat(2, t.getTaxa());
			 stmt.setFloat(3, t.getMulta());
			 stmt.setBoolean(4, t.isPago());
			 
			 java.sql.Timestamp data_pagamento = new java.sql.Timestamp(t.getData_pagamento().getTime());
			 stmt.setTimestamp(5, data_pagamento);
			 
			 stmt.setFloat(6, t.getPagarMulta());
			stmt.setInt(7, t.getId());
			stmt.executeUpdate();
			stmt.close();
			con.close();	
			
		} catch (SQLException e) {
			System.out.println(e.toString());
			throw new RuntimeException(e);
		}
		
		

		
	}

	@Override
	public Taxa getById(int id) throws Exception{
		PreparedStatement stmt= null;
		CondominoDAO dao = new CondominoDAO();
		open();
		try {
			
			
			stmt= con.prepareStatement("SELECT * FROM taxa WHERE id = ? ");
			stmt.setInt(1, id );
			
			ResultSet rs= stmt.executeQuery();
			if(rs.next()){
				Taxa t = new Taxa();
				t.setId(rs.getInt("id"));
				t.setTaxa(rs.getFloat("valor"));
				t.setCondomino(dao.getById(rs.getInt("id_condomino")));
				t.setData_pagamento(rs.getDate("data_pagamento"));
				t.setMulta(rs.getFloat("multa"));
				t.setPagarMulta(rs.getFloat("multa_pagar"));
				t.setPago(rs.getBoolean("pago"));
				t.setReferencia(rs.getDate("referencia"));
				
				return t;
			}
			con.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
			throw new RuntimeException(e);
		}
		
		con.close();
		return null;
		
	
		
	}

	@Override
	public List<Taxa> getAll() throws Exception {
		stmt= null;
		try {
			CondominoDAO dao = new CondominoDAO();
			List<Taxa> lista = new ArrayList<Taxa>();
			open();
			 stmt= con.prepareStatement("SELECT id, referencia, valor, multa," +
					"id_condomino, pago, data_pagamento, multa_pagar FROM taxa");
			ResultSet rs= stmt.executeQuery();
			while(rs.next()){
//				ColaboradorBuilder cb= new ColaboradorBuilder(rs.getInt(3));
//				cb.id(rs.getInt(1)).telefone(rs.getInt(2))
//				.servico(rs.getString(4));
//				lista.add(cb.create());
				Taxa t= new Taxa();
				t.setId(rs.getInt(1));
				t.setReferencia(rs.getDate(2));
				t.setTaxa(rs.getFloat(3));
				t.setMulta(rs.getFloat(4));
				t.setCondomino(dao.getById(rs.getInt(5)));
				t.setPago(rs.getBoolean(6));
				t.setData_pagamento(rs.getDate(7));
				t.setPagarMulta(rs.getFloat(8));
				lista.add(t);
			} 
			stmt.close();
			con.close();
			return lista;
			
			
		} catch (SQLException e) {
			System.out.println(e.toString());
		
		 }
		con.close();
		return null;
	}

	
	public void gerarTaxas(ArrayList<Condomino> list, Taxa t) throws Exception{
		for(Condomino c: list){
			t.setCondomino(c);
			this.add(t);
		}
	}
	public List<Taxa> getAnoByCondomino(final Condomino c,final int ano) throws Exception{
		List<Taxa> lista = new ArrayList<Taxa>();
		stmt= null;
		
		try{
			open();
			stmt= con.prepareStatement("SELECT year(referencia) as "+"'"+ ano+"'"+" , id, referencia, pago, valor, data_pagamento, multa_pagar," +
					" multa FROM taxa WHERE id_condomino = ? order by referencia");
			
			stmt.setInt(1, c.getId());
			ResultSet rs = stmt.executeQuery();
				while(rs.next()){
					Taxa t = new Taxa();
					t.setId(rs.getInt("id"));
					t.setReferencia(rs.getDate("referencia"));
					t.setMulta(rs.getFloat("multa"));
					t.setPago(rs.getBoolean("pago"));
					t.setPagarMulta(rs.getFloat("multa_pagar"));
					t.setData_pagamento(rs.getDate("data_pagamento"));
					t.setTaxa(rs.getFloat("valor"));
					t.setCondomino(c);
					lista.add(t);
					
					
					
				}
				
		} catch (SQLException e) {
			System.out.println(e.toString());
			//throw new RuntimeException(e);
		}
		stmt.close();
		con.close();
		return lista;
		
	}

	public List<Taxa> getMes(final int mes) throws Exception{
		List<Taxa> lista = new ArrayList<Taxa>();
		CondominoDAO dao = new CondominoDAO();
		stmt= null;
		
		try{
			open();
			stmt= con.prepareStatement("SELECT Month(referencia) as "+"'"+ mes+"'"+" , id, id_condomino, referencia, pago, valor, data_pagamento, multa_pagar," +
					" multa FROM taxa  order by referencia");
			
			
			ResultSet rs = stmt.executeQuery();
				while(rs.next()){
					Taxa t = new Taxa();
					t.setId(rs.getInt("id"));
					t.setReferencia(rs.getDate("referencia"));
					t.setMulta(rs.getFloat("multa"));
					t.setPago(rs.getBoolean("pago"));
					t.setPagarMulta(rs.getFloat("multa_pagar"));
					t.setData_pagamento(rs.getDate("data_pagamento"));
					t.setTaxa(rs.getFloat("valor"));
					t.setCondomino(dao.getById(rs.getInt("id_condomino")));
					lista.add(t);
					
					
					
				}
				
		} catch (SQLException e) {
			System.out.println(e.toString());
			//throw new RuntimeException(e);
		}
		stmt.close();
		con.close();
		return lista;
		
	}

}
