package DAO;

import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.sun.jmx.snmp.Timestamp;

import builder.ColaboradorBuilder;
import builder.CondominoBuilder;

import model.Colaborador;
import model.Condomino;
import model.Horarios;
import model.Veiculo;

import crud.Crud;

public class ColaboradorDAO extends DAO implements Crud {

	@Override
	public void add(Object crud) {
		
		
		PreparedStatement stmt;
		try {
			Colaborador c = (Colaborador) crud;
			System.out.println(c.toString());
			open();
			stmt= con.prepareStatement("INSERT INTO colaborador" +
					"(telefone, cpf, nome," +
					"servico) VALUES" +
					"(?,?,?,?)");
			stmt.setLong(1, c.getTelefone());
			stmt.setLong(2, c.getCpf());
			stmt.setString(3, c.getNome());
			stmt.setString(4, c.getServico());
			
			stmt.execute();
			
			
			stmt.close();
			con.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		}
		 catch (Exception e) {
				System.out.println(e.toString());
			}
		
	}

	@Override
	public void remove( Object crud) throws Exception {
		Colaborador c = (Colaborador) crud;
		PreparedStatement stmt= null;
		open();
		try {
			
			 stmt=  (PreparedStatement) con.prepareStatement("DELETE FROM colaborador" +
					"WHERE id = ?");
			stmt.setInt(1, c.getId());
			stmt.execute();
			stmt.close();
			con.close();
			
		} catch (SQLException e) {
			System.out.println(e.toString());
			throw new RuntimeException(e);
		}

		
	}

	@Override
	public void update(final Object crud)throws Exception {
		Colaborador c = (Colaborador) crud;
		PreparedStatement stmt= null;
		
		open();
		try {
		 stmt= (PreparedStatement) con.prepareStatement("UPDATE colaborador set nome = ?, cpf =? , telefone =? , servico = ?  WHERE id = ? ");
		stmt.setString(1, c.getNome());
		stmt.setLong(2, c.getCpf());
		stmt.setLong(3, c.getTelefone());
		stmt.setString(4, c.getServico());
		stmt.setInt(5, c.getId());
		stmt.executeUpdate();
		stmt.close();
		
			
			con.close();	
		
	} catch (SQLException e) {
		System.out.println(e.toString());
		throw new RuntimeException(e);
	}
	
	
		
	}

	@Override
	public Colaborador getById(final int id) throws Exception {
		PreparedStatement stmt= null;
		try {
			
			open();
			stmt= con.prepareStatement("SELECT * FROM colaborador WHERE id = ? ");
			stmt.setInt(1, id );
			
			ResultSet rs= stmt.executeQuery();
			if(rs.next()){
				Colaborador c = new Colaborador();
				c.setCpf(rs.getLong("cpf"));
				c.setId(rs.getInt("id"));
				c.setNome(rs.getString("nome"));
				c.setServico(rs.getString("servico"));
				c.setTelefone(rs.getLong("telefone"));
					
				stmt.close();
				con.close();
				return c;
			}
			con.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
			throw new RuntimeException(e);
		}
		stmt.close();
		con.close();
		return null;
		
	}
		
	

	@Override
	public List<Colaborador> getAll() throws Exception {
PreparedStatement stmt;
		
		try {
			List<Colaborador> lista = new ArrayList<Colaborador>();
			open();
			 stmt= con.prepareStatement("SELECT id, telefone, cpf, nome," +
					"servico FROM colaborador");
			ResultSet rs= stmt.executeQuery();
			while(rs.next()){
//				ColaboradorBuilder cb= new ColaboradorBuilder(rs.getInt(3));
//				cb.id(rs.getInt(1)).telefone(rs.getInt(2))
//				.servico(rs.getString(4));
//				lista.add(cb.create());
				Colaborador c= new Colaborador();
				c.setId(rs.getInt(1));
				c.setTelefone(rs.getLong(2));
				c.setCpf(rs.getLong	(3));
				c.setNome(rs.getString(4));
				c.setServico(rs.getString(5));
				lista.add(c);
			} 
			stmt.close();
			con.close();
			return lista;
			
			
		} catch (SQLException e) {
			System.out.println(e.toString());
		
		 }
		con.close();
		return null;
	}

	public void addAtendimento(Colaborador c, Horarios h, Condomino cond) throws Exception{
		PreparedStatement stmt = null;
		System.out.println(c.toString() + h.toString());
		try{	
			open();
			stmt= con.prepareStatement("INSERT INTO horarios ( horario, idColaborador, idCondomino)" +
					"VALUES(?,?, ?)");
			java.sql.Date dt= new java.sql.Date(h.getData().getTime());
			java.sql.Timestamp time= new java.sql.Timestamp(h.getData().getTime());
			System.out.println(dt);
			stmt.setTimestamp(1,  time);
			stmt.setInt(2, c.getId());
			stmt.setInt(3, cond.getId());
			
			
			stmt.execute();
			stmt.close();
			
		
		
			con.close();
			stmt.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		throw new RuntimeException(e);
	}
	
	con.close();
	
}
public List<Horarios> getHorarios(Colaborador c) throws Exception{
		CondominoDAO cdao= new CondominoDAO();
		ColaboradorDAO coldao= new ColaboradorDAO();
		PreparedStatement stmt;
		open();
		try{
			
			List<Horarios> lista = new ArrayList<Horarios>();
			
			stmt= con.prepareStatement("SELECT id, horario, idCondomino " +
					" FROM horarios WHERE idColaborador = "+c.getId());
			ResultSet rs= stmt.executeQuery();
			while(rs.next()){
				Horarios h = new Horarios();
				h.setId(rs.getInt(1));
				h.setData(rs.getTimestamp(2));
				h.setCondomino(cdao.getById(rs.getInt(3)));
				h.setColaborador(coldao.getById(c.getId()));
				
				
				
				lista.add(h);
			} 
			stmt.close();
			con.close();
			return lista;
			
		
		
		
	} catch (SQLException e) {
		System.out.println(e.toString());
	//throw new RuntimeException(e);
	
}
return null;


}
}
