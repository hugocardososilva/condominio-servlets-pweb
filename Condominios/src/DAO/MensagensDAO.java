
package DAO;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import model.Colaborador;
import model.Condomino;
import model.Mensagem;

import crud.Crud;

public class MensagensDAO extends DAO implements Crud{



	@Override
	public void add( final Object crud) throws Exception {
		PreparedStatement stmt;
		try {
			Mensagem m = (Mensagem) crud;
			
			System.out.println(m.toString());
			open();
			stmt= con.prepareStatement("INSERT INTO mensagem" +
					"(titulo, assunto, mensagem) VALUES" +
					"(?,?,?)");
			stmt.setString(1, m.getTitulo());
			stmt.setString(2, m.getAssunto() );
			stmt.setString(3, m.getMensagem());
			
			
			stmt.execute();
			stmt=null;
			
			ArrayList<Mensagem> aux= new ArrayList<Mensagem>();
			aux =  (ArrayList<Mensagem>) getAll();
			Mensagem men= aux.get(aux.size()-1);
			System.out.println(men.getId() + "  id da mensagem");
			open();
			
			for(Condomino c : m.getDestinatarios()){
				stmt=null;
				stmt= con.prepareStatement("INSERT INTO destinatatios (" +
						"id_mensagem, id_condomino, lido) VALUES(" +
						"?,?,?)");
				stmt.setInt(1, men.getId());
				stmt.setInt(2, c.getId());
				stmt.setBoolean(3, false);
				stmt.execute();
				stmt.close();
				stmt= null;
			}
			
			
			stmt.close();
			con.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
		}
		 catch (Exception e) {
				System.out.println(e.toString());
			}
		
		
	}

	@Override
	public void remove(Object crud) throws Exception {
		// TODO Auto-generated method stub
		
	}


	public void lerMensagem(final int mensagem,final int condomino) throws Exception {
		PreparedStatement stmt= null;
	
		open();
		try {
		 stmt= (PreparedStatement) con.prepareStatement("UPDATE destinatatios set lido = ? WHERE id_condomino = ? AND id_mensagem = ? ");
		stmt.setInt(1, 1);
		stmt.setInt(3, mensagem);
		stmt.setInt(2, condomino);
		
		stmt.executeUpdate();
		//System.out.println(stmt.toString());
		stmt.close();	
		} catch (SQLException e) {
			System.out.println(e.toString());
			throw new RuntimeException(e);
		}
		con.close();
		
	}
	public void excluirMensagem(final int mensagem,final int condomino) throws Exception {
		PreparedStatement stmt= null;
	
		open();
		try {
		 stmt= (PreparedStatement) con.prepareStatement("DELETE FROM destinatatios WHERE id_mensagem=? and id_condomino=?");
		stmt.setInt(1, mensagem);
		
		stmt.setInt(2, condomino);
		
		stmt.executeUpdate();
		
		//System.out.println(stmt.toString());
		stmt.close();	
		con.close();
		} catch (SQLException e) {
			System.out.println(e.toString());
			con.close();
			
		}
		
		
	}

	@Override
	public Object getById(int id) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Mensagem> getAll() throws Exception {
PreparedStatement stmt;
		
		try {
			List<Mensagem> lista = new ArrayList<Mensagem>();
			open();
			 stmt= con.prepareStatement("SELECT *" +
					"FROM mensagem");
			ResultSet rs= stmt.executeQuery();
			while(rs.next()){
				Mensagem m = new Mensagem();
				m.setAssunto(rs.getString("assunto"));
				m.setTitulo(rs.getString("titulo"));
				m.setId(rs.getInt("id"));
				m.setMensagem(rs.getString("mensagem"));
					stmt=null;
					stmt= con.prepareStatement("SELECT  id_mensagem, id_condomino," +
							"lido FROM destinatatios where id_mensagem = ?");
					stmt.setInt(1, m.getId());
					ResultSet rs2= stmt.executeQuery();
					ArrayList<Condomino> condominos= new ArrayList<Condomino>();
					while(rs2.next()){
						CondominoDAO daoCon= new CondominoDAO();
						Condomino c = new Condomino();
						c= daoCon.getById(rs2.getInt("id_condomino"));
						condominos.add(c);
					}
					m.setDestinatarios(condominos);
				lista.add(m);
			} 
			stmt.close();
			con.close();
			return lista;
			
			
		} catch (SQLException e) {
			System.out.println(e.toString());
		
		 }
		con.close();
		return null;
	}
	
	public List<Mensagem> getMinhasMensagens(final int idCondomino) throws Exception {
		PreparedStatement stmt;
				
				try {
					List<Mensagem> lista = new ArrayList<Mensagem>();
					open();
					 stmt= con.prepareStatement("SELECT *" +
							"FROM mensagem");
					ResultSet rs= stmt.executeQuery();
					while(rs.next()){
						Mensagem m = new Mensagem();
						m.setAssunto(rs.getString("assunto"));
						m.setTitulo(rs.getString("titulo"));
						m.setId(rs.getInt("id"));
						m.setMensagem(rs.getString("mensagem"));
							stmt=null;
							stmt= con.prepareStatement("SELECT  id_mensagem, id_condomino," +
									"lido FROM destinatatios where id_mensagem = ?");
							stmt.setInt(1, m.getId());
							ResultSet rs2= stmt.executeQuery();
							ArrayList<Condomino> condominos= new ArrayList<Condomino>();
							while(rs2.next()){
								CondominoDAO daoCon= new CondominoDAO();
								Condomino c = new Condomino();
								c= daoCon.getById(rs2.getInt("id_condomino"));
								if(idCondomino == c.getId()){
									m.setLido(rs2.getBoolean("lido"));
									lista.add(m);
								}
								//condominos.add(c);
							}
							//m.setDestinatarios(condominos);
						
					} 
					stmt.close();
					con.close();
					return lista;
					
					
				} catch (SQLException e) {
					System.out.println(e.toString());
				
				 }
				con.close();
				return null;
			}

	@Override
	public void update(Object crud) throws Exception {
		// TODO Auto-generated method stub
		
	}

}
