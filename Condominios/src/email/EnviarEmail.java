package email;

import java.util.Properties;

import javax.mail.AuthenticationFailedException;
import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.ServletContext;



public class EnviarEmail {
	
	
	public String Enviar(String para, String assunto, String titulo, ServletContext sc){
		String host= sc.getInitParameter("host");
		final String emailEnvio= sc.getInitParameter("emailSite");
		final String senha= sc.getInitParameter("senha");
		String porta= sc.getInitParameter("porta");
		
		try {
			Properties props= new Properties();
			props.setProperty("mail.host", host);
            props.setProperty("mail.smtp.port", porta);
            props.setProperty("mail.smtp.auth", "true");
//            props.setProperty("mail.smtp.starttls.enable", "true");
           
            Session session= Session.getInstance(props, new Authenticator() {
            	protected PasswordAuthentication getPasswordAuthentication(){
            		return new PasswordAuthentication(emailEnvio, senha);
            	}
			});
            MimeMessage mensagem= new MimeMessage(session);
            mensagem.setText(assunto);
            mensagem.setSubject(assunto);
            mensagem.setFrom(new InternetAddress(emailEnvio));
            mensagem.addRecipient(Message.RecipientType.TO, new InternetAddress(para));
            Transport.send(mensagem);
            
           
            
		} catch (AuthenticationFailedException ex) {
            return "Authentication failed";

           

        } catch (AddressException ex) {
            return "Wrong email address";

           

        } catch (MessagingException ex) {
            return ex.getMessage();

           
        }
		 
		 return "Email enviado com sucesso";
	
	
	}

}
